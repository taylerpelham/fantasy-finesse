from django.apps import AppConfig


class LineupsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'lineups'
    def ready(self):
        from . import signals
