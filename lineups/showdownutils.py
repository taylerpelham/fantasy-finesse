
from .models import DraftPool, DraftPlayer, ClassicLineup, LineupGroup
import django
from draftfast.orm import Player
from draftfast import rules
from draftfast.optimize import run, run_multi
from draftfast.lineup_constraints import LineupConstraints
import requests
from draftfast.csv_parse.upload import _on_position
from draftfast.rules import DRAFT_KINGS
from draftfast.showdown.orm import ShowdownPlayer

"""
The following 2 funcitons Allow us to change the player_pool to a showdown player pool
"""

def capt_boost(p):
    player = Player(
        name=p.name,
        pos=p.pos,
        cost=p.cost * 1.5,
        proj=round(p.proj * 1.5, 2),
        team=p.team,
        possible_positions=p.possible_positions
    )
    return player

def add_capts(player_pool, captain):
    ###Make a pool with each player as a captain with 1.5x stats and make all players showdownplayers
    captain_pool = [capt_boost(p) for p in player_pool]
    pool = []
    for p in player_pool:
        pool.append(ShowdownPlayer(p))
    if captain:
        c = capt_boost(captain[0])
        pool.append(ShowdownPlayer(c, captain=True))
    else:
        for p in captain_pool:
            pool.append(ShowdownPlayer(p, captain=True))
    return pool


## edit this to make spefifc for showdown
def csv_order(roster, rules_id, game=DRAFT_KINGS):
    """
    Takes Roster Object as Input and Returns an ordered List of ID in draftkings order
    """
    players = roster
    if game == DRAFT_KINGS and rules_id == 70:
        ordered_possible = [
        _on_position(players, ["PG"]),
        _on_position(players, ["SG"]),
        _on_position(players, ["SF"]),
        _on_position(players, ["PF"]),
        _on_position(players, ["C"]),
        _on_position(players, ["SG", "PG"]),
        _on_position(players, ["SF", "PF"]),
        players,
    ]
    elif game == DRAFT_KINGS and rules_id in [1, 158]:
        ordered_possible = [
            _on_position(players, ["QB"]),
            _on_position(players, ["RB"]),
            _on_position(players, ["RB"]),
            _on_position(players, ["WR"]),
            _on_position(players, ["WR"]),
            _on_position(players, ["WR"]),
            _on_position(players, ["TE"]),
            # NFL Flex
            _on_position(players, ["TE", "WR", "RB"]),
            _on_position(players, ["DST"]),
        ]
    #NBA Showdown
    elif game == DRAFT_KINGS and rules_id in [81, 159, 96]:
        p = [p.name for p in players]
        return p### needs to return showdown list order

    ordered_lineup = []
    counter = 0
    for ps in ordered_possible:
        counter += 1
        not_used_ps = [p for p in ps if p not in ordered_lineup]
        ordered_lineup.append(not_used_ps[0])
    ordered_ids = []
    for play in ordered_lineup:
        ordered_ids.append(play.name)
    return ordered_ids



"""
The following functions will be using the draftfast package to enter the players to be crunched for showdown mode
"""


def showdown_opt(formset, lineup_group, iterations=5):
    """
    Input from PlayerFormSet
    """
    locks = []
    excluded_players = []
    exposure_bounds = []
    player_pool = []
    captain = []
    for form in formset:
        if '/' in form['position']:
            positions = form['position'].split('/')
            pos = positions[0]
        else:
            pos = form['position']
        if form['captain'] == True:
            captain.append(Player(
                name=str(form['player_id']),
                cost=form['salary'],
                proj=form['fppg'],
                pos=pos,
                possible_positions=form['position'],
                lock=True
                   ))
        if form['exclude'] == True:
            excluded_players.append(str(form['player_id']))
        if form['lock']:
            locks.append(str(form['player_id']))
        if form['min_exp'] != 0 or form['max_exp'] != 100:
            min = float(form['min_exp'])/100
            max = float(form['max_exp'])/100
            exposure_bounds.append({
                'name':str(form['player_id']),
                'min': min,
                'max': max,
            })

        player_pool.append(
            Player(
                name=str(form['player_id']),
                cost=form['salary'],
                proj=form['fppg'],
                team=form['team'],
                pos=pos,
                possible_positions=form['position'],
                   )
            )
    constraints = LineupConstraints(
        locked=locks,
        banned=excluded_players,

    )
    #currently passing this to make csv
    rules_id = lineup_group.draft_pool.contest_type_id
    ruleset = rules.DK_NBA_SHOWDOWN_RULE_SET
    player_pool = add_capts(player_pool, captain)

    rosters = run_multi(
        iterations=iterations,
        rule_set=ruleset,
        player_pool=player_pool,
        verbose=False,
        constraints=constraints,
        exposure_bounds=exposure_bounds,
    )

    for roster in rosters[0]:
        ## Sorted draftkings roster stuff

        ros = roster.sorted_players()
        order = csv_order(roster=ros, rules_id=rules_id)
        projected_points = roster.projected()
        salary = roster.spent()
        players = []
        locks = []
        cpt = []
        for player in roster.players:
            draft_player = DraftPlayer.objects.get(draft_group=lineup_group.draft_pool.draft_group_id, draft_id=int(player.name))###this needs draft_player_group
            if player.pos == 'CPT':
                cpt.append(draft_player)
            elif player.lock == True:
                locks.append(draft_player)
            else:
                players.append(draft_player)
        playerset = {player for player in players}
        lockset = {lock for lock in locks}
        lineup = ClassicLineup.objects.create(projected_points=projected_points, lineup_group=lineup_group, total_salary=salary, order=order, cpt=cpt[0])
        for player in playerset:
            lineup.players.add(player)
        for lock in lockset:
            lineup.locks.add(lock)
