from django.shortcuts import render, get_object_or_404, redirect
from .models import DraftPool, DraftPlayer
from .forms import PlayerOptimizationForm, ShowdownOptimizationForm
from django.forms import formset_factory
from .models import LineupGroup
from .utils import get_player_pool
from .showdownutils import showdown_opt
import csv
from django.http import HttpResponse
import ast
from django.utils import timezone


def home(request):
    return render(request, 'lineups/home.html')


def draftpool_list(request):###TODO: Return the last 30/last days Draftpools
    now = timezone.now()
    draftpools = DraftPool.objects.filter(contest_type_id=81, starts_at__gt=now, starts_at__lt=now + timezone.timedelta(hours=30)).order_by('starts_at')
    context = {"draftpools": draftpools}
    return render(request, 'lineups/draftpools_list.html', context)


def classic_draftpool_list(request):###TODO: Return the last 30/last days Draftpools
    now = timezone.now()
    draftpools = DraftPool.objects.filter(contest_type_id=70, starts_at__gt=now, starts_at__lt=now + timezone.timedelta(hours=30))
    context = {"draftpools": draftpools}
    return render(request, 'lineups/draftpools_list.html', context)


def showdown_draftpool_list(request):###TODO: Return the last 30/last days Draftpools
    now = timezone.now()
    draftpools = DraftPool.objects.filter(contest_type_id=81, starts_at__gt=now, starts_at__lt=now + timezone.timedelta(hours=30))
    context = {"draftpools": draftpools}
    return render(request, 'lineups/draftpools_list.html', context)


def optimize_players(request, draft_group_id):
    draft_group = DraftPool.objects.get(draft_group_id=draft_group_id)
    players = DraftPlayer.objects.filter(draft_group__draft_group_id=draft_group_id, points_per_game__gt=0)
    if draft_group.contest_type_id in [81, 159, 96]:
        form = ShowdownOptimizationForm
    else:
        form = PlayerOptimizationForm
    PlayerFormSet = formset_factory(form, extra=0)

    if request.method == 'POST':
        formset = PlayerFormSet(request.POST)

        if formset.is_valid():
            #print(formset.cleaned_data)
            iterations = int(request.POST.get('num_lineups'))
            lineup_group = LineupGroup.objects.create(draft_pool=draft_group)
            formset_data = formset.cleaned_data
            if draft_group.contest_type_id in [81, 159, 96]:
                showdown_opt(formset_data, lineup_group, iterations)
            else:
                get_player_pool(formset_data, lineup_group, iterations)
            return redirect('lineup_group', lineup_group.id)

        else:
            print(formset.errors)
    else:
        initial_data = [{
            'player_id': player.draft_id,## this will clase confusion later missed name everywhere else
            'name': player.name,
            'fppg': player.points_per_game,
            'team': player.team_abv,
            'salary': int(player.salary),
            'position': player.position,
            'status': player.status,
            'exclude': True if player.status == 'OUT' else False
            } for player in players]
        formset = PlayerFormSet(initial=initial_data)

    context = {
        'formset': formset,
        'draftpool': draft_group
               }

    return render(request, 'lineups/cruncher_input.html', context)

def lineup_group_detail(request, lineup_group_id):
    lineups = LineupGroup.objects.get(id=lineup_group_id)
    context = {'lineups': lineups}
    return render(request, 'lineups/lineup_group.html', context)


def lineups_csv(request, lineup_group_id):
    lineups = LineupGroup.objects.get(id=lineup_group_id)
    response = HttpResponse(
        content_type='text/csv',
        headers={"Content-Disposition": 'attachment; filename="lineups.csv"'},
    )
    writer = csv.writer(response)

    if lineups.draft_pool.contest_type_id == 70:
        writer.writerow(['PG', 'SG', 'SF', 'PF', 'C', 'G', 'F', 'UTIL'])
    elif lineups.draft_pool.contest_type_id in [1, 158]:
        writer.writerow(["QB", "RB", "RB", "WR", "WR", "WR", "TE", "FLEX", "DST"])
    elif lineups.draft_pool.contest_type_id in [81]:
        writer.writerow(["CPT", "UTIL", "UTIL", "UTIL", "UTIL", "UTIL"])
    elif lineups.draft_pool.contest_type_id in [159, 96]:
        writer.writerow(["CPT", "FLEX", "FLEX", "FLEX", "FLEX", "FLEX"])

    for lineup in lineups.lineup_group.all():
        player_list = ast.literal_eval(lineup.order)
        writer.writerow(player_list)

    return response
