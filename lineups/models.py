from django.db import models

# Create your models here.

class DraftPool(models.Model):
    draft_group_id = models.BigIntegerField(primary_key=True)
    contest_type_id = models.IntegerField(default=0, null=True, blank=True)### GAME_TYPE_ID from draftkings
    games_count = models.IntegerField(default=0, null=True, blank=True)
    sport = models.CharField(max_length=100, null=True, blank=True)
    starts_at = models.DateTimeField()


class DraftPlayer(models.Model):
    draft_group = models.ForeignKey(DraftPool, related_name='draft_players',on_delete=models.CASCADE)
    player_id = models.IntegerField(default=0, null=True, blank=True)
    draft_id = models.IntegerField(default=0, null=True, blank=True)
    name = models.CharField(max_length=100)
    salary = models.FloatField(default=0, null=True, blank=True)
    points_per_game = models.FloatField(default=0, null=True, blank=True)
    position = models.CharField(max_length=100)
    team_abv = models.CharField(max_length=10, null=True, blank=True)
    game_name = models.CharField(max_length=20, null=True, blank=True)
    opposition_rank = models.IntegerField(default=0, null=True, blank=True)
    status = models.CharField(max_length=100)
    def __str__(self):
        return self.name

    class Meta:
        unique_together = ["draft_group", "player_id"]


class LineupGroup(models.Model):
    draft_pool = models.ForeignKey(DraftPool, related_name="lineup_group", on_delete=models.CASCADE)#display this as PlayerOptimizationForm
    #iterations = models.IntegerField(null=True, blank=True)#of lineups to generate


class ClassicLineup(models.Model):
    players = models.ManyToManyField(DraftPlayer, related_name='lineup_players')
    projected_points = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True)
    total_salary = models.IntegerField(null=True, blank=True)
    locks = models.ManyToManyField(DraftPlayer, related_name='lineup_locks')
    lineup_group = models.ForeignKey(LineupGroup, related_name='lineup_group', on_delete=models.CASCADE)
    order = models.CharField(max_length=300, null=True, blank=True)
    cpt = models.ForeignKey(DraftPlayer, related_name='lineup_cpt', on_delete=models.CASCADE, blank=True, null=True)
