from django import forms
from django.forms.widgets import Input

class DisplayInput(Input): ## inherits from Input widget and passes a different HTML back
    input_type = 'text'
    template_name = 'lineups/displayinput.html'


class PlayerOptimizationForm(forms.Form):
    player_id = forms.IntegerField(widget=forms.HiddenInput(), required=False)
    name = forms.CharField(required=False, widget=DisplayInput())
    salary = forms.IntegerField(widget=DisplayInput())
    position = forms.CharField(required=False, widget=DisplayInput(attrs={'class':'input-group mb-1'}))
    status = forms.CharField(required=False, widget=DisplayInput(attrs={'class':'input-group mb-1'}))
    fppg = forms.FloatField(widget=forms.NumberInput(attrs={'style':"max-width: 65px;"}))
    lock = forms.BooleanField(initial=False, required=False)
    exclude = forms.BooleanField(initial=False, required=False)
    min_exp = forms.IntegerField(min_value=0, max_value=100, initial=0, widget=forms.NumberInput(attrs={'style':"max-width: 50px;"}))
    max_exp = forms.IntegerField(min_value=0, max_value=100, initial=100, widget=forms.NumberInput(attrs={'style':"max-width: 50px;"}))
    team = forms.CharField(required=False, widget=DisplayInput(attrs={'class':'input-group mb-1'}))


class ShowdownOptimizationForm(PlayerOptimizationForm):
    captain = forms.BooleanField(initial=False, required=False)
