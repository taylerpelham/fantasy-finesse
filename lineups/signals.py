from django.db.models.signals import post_save
from django.dispatch import receiver
from .models import DraftPool, DraftPlayer

@receiver(post_save, sender=DraftPool)
def create_draft_players(sender, instance, created, **kwargs):
    """
    Signal to create DraftPlayer instances when a new DraftPool is created.
    """
    if created:
        pass
        #created DraftPlayers
