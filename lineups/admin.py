from django.contrib import admin
from .models import DraftPool, DraftPlayer, LineupGroup, ClassicLineup
@admin.register(DraftPool)
class DraftPoolAdmin(admin.ModelAdmin):
    list_display = ['draft_group_id', 'starts_at', 'contest_type_id']

@admin.register(DraftPlayer)
class DraftPlayerAdmin(admin.ModelAdmin):
    list_display = ['name','draft_id', 'salary', 'position']

@admin.register(LineupGroup)
class LineupGroupAdmin(admin.ModelAdmin):
    list_display = ['draft_pool']

@admin.register(ClassicLineup)
class ClassicLineupAdmin(admin.ModelAdmin):
    list_display = ['projected_points', 'total_salary', 'lineup_group']

