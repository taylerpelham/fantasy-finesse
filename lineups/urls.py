from django.urls import path
from .views import optimize_players, lineup_group_detail, draftpool_list, lineups_csv, classic_draftpool_list,showdown_draftpool_list, home


urlpatterns = [
    path('create/<int:draft_group_id>', optimize_players, name="create_lineups"),
    path('lineup_group/<int:lineup_group_id>', lineup_group_detail, name='lineup_group'),
    path('draftpools/', draftpool_list, name='draftpool_list'),
    path('lineups_csv/<int:lineup_group_id>', lineups_csv, name='lineups_csv'),
    path('draftpools/classic', classic_draftpool_list, name='classic_draftpool_list'),
    path('draftpools/showdown', showdown_draftpool_list, name='showdown_draftpool_list'),
]
