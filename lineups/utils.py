from .models import DraftPool, DraftPlayer, ClassicLineup, LineupGroup
import django
from draftfast.orm import Player
from draftfast import rules
from draftfast.optimize import run, run_multi
from draftfast.lineup_constraints import LineupConstraints
import requests
from draftfast.csv_parse.upload import _on_position
from draftfast.rules import DRAFT_KINGS
from draftfast.showdown.orm import ShowdownPlayer

"""
Funcitons to Update DraftPlayers before game Starts
"""
def update_players(draft_pool_id):
    contests_response = requests.get(f'https://api.draftkings.com/draftgroups/v1/draftgroups/{draft_pool_id}/draftables')
    contests_json = contests_response.json()
    draft_pool = DraftPool.objects.get(draft_group_id=draft_pool_id)
    players = DraftPlayer.objects.filter(draft_group=draft_pool)
    for player in contests_json['draftables']:
        if len(player['playerGameAttributes']) != 0 or player['status'] != 'None':
            try:
                dp = DraftPlayer.objects.get(draft_id=player['draftableId'])
                if len(player['playerGameAttributes']) != 0:
                    dp.status = 'Starting'
                    dp.save()
                elif player['status'] != 'None':
                    dp.status = player['status']
                    dp.save()
            except:
                continue

"""
Functions to make DraftPools and DraftPlayer from Draftkings API
"""
def get_players_draftpools(draft_pool_id):
    contests_response = requests.get(f'https://api.draftkings.com/draftgroups/v1/draftgroups/{draft_pool_id}/draftables')
    contests_json = contests_response.json()
    draft_pool = DraftPool.objects.get(draft_group_id=draft_pool_id)
    for player in contests_json['draftables']:
        if draft_pool.contest_type_id in  [81,159]:
            if player['rosterSlotId'] in [475, 512]:
                try:
                    DraftPlayer.objects.create(
                    draft_group=draft_pool,
                    draft_id=player["draftableId"],
                    player_id=player["playerId"],
                    name=player['displayName'],
                    position=player['position'],
                    salary=player["salary"] ,
                    status=player['status'],
                    points_per_game=player['draftStatAttributes'][0]['value'],
                    opposition_rank=player['draftStatAttributes'][1]['sortValue'] if player['draftStatAttributes'][1]['sortValue'] != '' else 0,
                    team_abv=player["teamAbbreviation"],
                    game_name=player["competition"]['name'])
                except (django.db.utils.IntegrityError, KeyError, ValueError):
                    continue
        else:
            try:
                DraftPlayer.objects.create(
                draft_group=draft_pool,
                draft_id=player["draftableId"],
                player_id=player["playerId"],
                name=player['displayName'],
                position=player['position'],
                salary=player["salary"] ,
                status=player['status'],
                points_per_game=player['draftStatAttributes'][0]['value'],
                opposition_rank=player['draftStatAttributes'][1]['sortValue'] if player['draftStatAttributes'][1]['sortValue'] != '' else 0,
                team_abv=player["teamAbbreviation"],
                game_name=player["competition"]['name'])
            except (django.db.utils.IntegrityError, KeyError, ValueError):
                continue


def update_draftpools(key='NBA'):
    contests_response = requests.get(f'https://www.draftkings.com/lobby/getcontests?sport={key}')
    contests_json = contests_response.json()
    print('udate these piips')
    for group in contests_json['DraftGroups']:
        if group['GameTypeId'] in [1,70, 81, 158, 96, 159]:
            try:
                draftpool = DraftPool.objects.create(
                draft_group_id=group["DraftGroupId"],
                contest_type_id=group["GameTypeId"],
                starts_at=group["StartDateEst"],
                sport=group["Sport"],
                games_count=group["GameCount"])
                if draftpool:
                    get_players_draftpools(draft_pool_id=draftpool.draft_group_id)
            except (django.db.utils.IntegrityError, KeyError, ValueError):
                continue
        else:
            continue


def csv_order(roster, rules_id, game=DRAFT_KINGS):
    """
    Takes Roster Object as Input and Returns an ordered List of ID in draftkings order
    """
    players = roster
    if game == DRAFT_KINGS and rules_id == 70:
        ordered_possible = [
        _on_position(players, ["PG"]),
        _on_position(players, ["SG"]),
        _on_position(players, ["SF"]),
        _on_position(players, ["PF"]),
        _on_position(players, ["C"]),
        _on_position(players, ["SG", "PG"]),
        _on_position(players, ["SF", "PF"]),
        players,
    ]
    elif game == DRAFT_KINGS and rules_id in [1, 158]:
        ordered_possible = [
            _on_position(players, ["QB"]),
            _on_position(players, ["RB"]),
            _on_position(players, ["RB"]),
            _on_position(players, ["WR"]),
            _on_position(players, ["WR"]),
            _on_position(players, ["WR"]),
            _on_position(players, ["TE"]),
            # NFL Flex
            _on_position(players, ["TE", "WR", "RB"]),
            _on_position(players, ["DST"]),
        ]
    #NBA Showdown
    elif game == DRAFT_KINGS and rules_id in [81, 159, 96]:
        p = [p.name for p in players]
        return p### needs to return showdown list order

    ordered_lineup = []
    counter = 0
    for ps in ordered_possible:
        counter += 1
        not_used_ps = [p for p in ps if p not in ordered_lineup]
        ordered_lineup.append(not_used_ps[0])
    ordered_ids = []
    for play in ordered_lineup:
        ordered_ids.append(play.name)
    return ordered_ids


"""
The following functions will be using the draftfast package to enter the players to be crunched
"""


def get_player_pool(formset, lineup_group, iterations=5):
    """
    Input from PlayerFormSet
    """
    locks = []
    excluded_players = []
    exposure_bounds = []
    player_pool = []
    captain = []
    for form in formset:
        if '/' in form['position']:
            positions = form['position'].split('/')
            pos = positions[0]
        else:
            pos = form['position']
        if form['exclude'] == True:
            excluded_players.append(str(form['player_id']))
        if form['lock']:
            locks.append(str(form['player_id']))
        if form['min_exp'] != 0 or form['max_exp'] != 100:
            min = float(form['min_exp'])/100
            max = float(form['max_exp'])/100
            exposure_bounds.append({
                'name':str(form['player_id']),
                'min': min,
                'max': max,
            })

        player_pool.append(
            Player(
                name=str(form['player_id']),
                cost=form['salary'],
                proj=form['fppg'],
                team=form['team'],
                pos=pos,
                possible_positions=form['position'],
                   )
            )
    constraints = LineupConstraints(
        locked=locks,
        banned=excluded_players,
    )
    rules_id = lineup_group.draft_pool.contest_type_id
    #set rule set
    if rules_id in [1, 158]:
        ruleset = rules.DK_NFL_RULE_SET
    else:
        ruleset = rules.DK_NBA_RULE_SET
    rosters = run_multi(
        iterations=iterations,
        rule_set=ruleset,
        player_pool=player_pool,
        verbose=False,
        constraints=constraints,
        exposure_bounds=exposure_bounds,
    )
    for roster in rosters[0]:
        ## Sorted draftkings roster stuff
        ros = roster.sorted_players()
        order = csv_order(roster=ros, rules_id=rules_id)
        projected_points = roster.projected()
        salary = roster.spent()
        players = []
        locks = []
        for player in roster.players:
            draft_player = DraftPlayer.objects.get(draft_group=lineup_group.draft_pool.draft_group_id, draft_id=int(player.name))###this needs draft_player_group
            if player.lock == True:
                locks.append(draft_player)
            else:
                players.append(draft_player)
        playerset = {player for player in players}
        lockset = {lock for lock in locks}
        lineup = ClassicLineup.objects.create(projected_points=projected_points, lineup_group=lineup_group, total_salary=salary, order=order)
        for player in playerset:
            lineup.players.add(player)
        for lock in lockset:
            lineup.locks.add(lock)
