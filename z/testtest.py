import http.client
import json
#from nba.models import Player, Team, Game
conn = http.client.HTTPSConnection("api-nba-v1.p.rapidapi.com")

headers = {
    'X-RapidAPI-Key': "b99d40bbc6msh3aa3e31f97d65b8p1d29ddjsnbf5d620b0880",
    'X-RapidAPI-Host': "api-nba-v1.p.rapidapi.com"
}

conn.request("GET", "/players/statistics?game=13013", headers=headers)

res = conn.getresponse()
data = res.read()

dict1 = json.loads(data.decode("utf-8"))

print(dict1)
