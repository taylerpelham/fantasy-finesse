import requests
from datetime import datetime
import json
import http.client

"""
Test funcitons for prop-odds.com
"""

BASE_URL = 'api.prop-odds.com'
API_KEY ='VflzKVPQcRUYpdKMJQLbHazCJhi1wCwl5gLu99T1T0o'

def today_game_props(date=datetime.today().strftime('%Y-%m-%d')):
  """
  Given a Date returns Game IDs for the date in NBA
  """
  conn = http.client.HTTPSConnection(BASE_URL)
  conn.request("GET", f'/beta/games/nba?date={date}&tz=America/New_York&api_key={API_KEY}')
  res = conn.getresponse()
  data = res.read()
  dict1 = json.loads(data.decode("utf-8"))
  return dict1
"https://api.prop-odds.com/beta/markets/{game_id}"
"https://api.prop-odds.com/v1/fantasy_snapshot/{league}/{market}"

def get_game_markets(game_id='aedde43e243c45b078af131f3079cce5'):
  conn = http.client.HTTPSConnection(BASE_URL)
  conn.request("GET", f'/beta/markets/{game_id}?api_key={API_KEY}')
  res = conn.getresponse()
  data = res.read()
  dict1 = json.loads(data.decode("utf-8"))
  return dict1


def game_props(game_id='aedde43e243c45b078af131f3079cce5'):
  conn = http.client.HTTPSConnection(BASE_URL)
  market = 'player_assists_points_rebounds_over_under'
  conn.request("GET", f'beta/odds/9270370f57e92a0754c3b5c763f6301f/moneyline?api_key={API_KEY}')
  res = conn.getresponse()
  data = res.read()
  #dict1 = json.loads(data.decode("utf-8"))
  return data

print(game_props())
