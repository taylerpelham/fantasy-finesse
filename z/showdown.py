from draftfast.orm import Player
from draftfast.showdown.orm import ShowdownPlayer
from draftfast import rules
from draftfast.optimize import run, run_multi
from draftfast.lineup_constraints import LineupConstraints
from draftfast.csv_parse.upload import _on_position
from draftfast.rules import DRAFT_KINGS



formsets = [{'player_id': 32122136, 'name': 'Joel Embiid', 'salary': 15600, 'position': 'C', 'status': '', 'fppg': 64.9, 'lock': False, 'exclude': False, 'min_exp': 0, 'max_exp': 100, 'team': 'PHI', 'captain': False}, {'player_id': 32122137, 'name': 'Tyrese Maxey', 'salary': 13200, 'position': 'PG', 'status': '', 'fppg': 44.3,
'lock': False, 'exclude': False, 'min_exp': 0, 'max_exp': 100, 'team': 'PHI', 'captain': False}, {'player_id': 32122138, 'name': 'DeMar DeRozan', 'salary': 12600, 'position': 'SF/PF', 'status': '', 'fppg': 38.9, 'lock': False, 'exclude': False, 'min_exp': 0, 'max_exp': 100, 'team': 'CHI', 'captain': False}, {'player_id': 32122139, 'name': 'Andre Drummond', 'salary': 12300, 'position': 'C', 'status': '', 'fppg': 21.0, 'lock': False, 'exclude': False, 'min_exp': 0, 'max_exp': 100, 'team': 'CHI', 'captain': False}, {'player_id': 32122140, 'name': 'Nikola Vucevic', 'salary': 12000, 'position': 'C', 'status': '', 'fppg': 38.4, 'lock': False, 'exclude': True, 'min_exp': 0, 'max_exp': 100, 'team': 'CHI', 'captain': False}, {'player_id': 32122141, 'name': 'Coby White', 'salary': 11400, 'position': 'PG', 'status': '', 'fppg': 32.6, 'lock': False, 'exclude': False, 'min_exp': 0, 'max_exp': 100, 'team': 'CHI', 'captain': False}, {'player_id': 32122142, 'name': 'Tobias Harris', 'salary': 11100, 'position': 'SF/PF', 'status': '', 'fppg': 32.3, 'lock': False, 'exclude': False, 'min_exp': 0, 'max_exp': 100, 'team': 'PHI', 'captain': False}, {'player_id': 32122143, 'name': 'Zach LaVine', 'salary': 10800, 'position': 'SG/SF', 'status': '', 'fppg': 34.7, 'lock': False,
'exclude': True, 'min_exp': 0, 'max_exp': 100, 'team': 'CHI', 'captain': False}, {'player_id': 32122144, 'name': "De'Anthony Melton", 'salary': 10200, 'position': 'PG/SG', 'status': '', 'fppg': 26.8, 'lock': False, 'exclude': True, 'min_exp': 0, 'max_exp': 100, 'team': 'PHI', 'captain': False}, {'player_id': 32122145, 'name': 'Paul Reed', 'salary': 9600, 'position': 'C', 'status': '', 'fppg': 15.4, 'lock': False, 'exclude': False, 'min_exp': 0, 'max_exp': 100, 'team': 'PHI', 'captain': False}, {'player_id': 32122146, 'name': 'Kelly Oubre Jr.', 'salary': 9300, 'position': 'SG/SF', 'status': '', 'fppg': 23.0, 'lock': False, 'exclude': False, 'min_exp': 0, 'max_exp': 100, 'team': 'PHI', 'captain': False}, {'player_id': 32122147, 'name': 'Patrick Williams', 'salary': 9000, 'position': 'SF/PF', 'status': '', 'fppg': 21.3, 'lock': False, 'exclude': False, 'min_exp': 0, 'max_exp': 100, 'team': 'CHI', 'captain': False}, {'player_id': 32122148, 'name': 'Alex Caruso', 'salary': 8700, 'position': 'SG/SF', 'status': '', 'fppg': 22.2, 'lock': False, 'exclude': False, 'min_exp': 0, 'max_exp': 100, 'team': 'CHI', 'captain': False}, {'player_id': 32122149, 'name': 'Ayo Dosunmu', 'salary': 6900, 'position': 'PG/SG', 'status': '', 'fppg': 15.7, 'lock': False, 'exclude': False, 'min_exp': 0, 'max_exp': 100, 'team': 'CHI', 'captain': False}, {'player_id': 32122150, 'name': 'Mo Bamba', 'salary': 6600, 'position': 'C', 'status':
'', 'fppg': 10.0, 'lock': False, 'exclude': True, 'min_exp': 0, 'max_exp': 100, 'team': 'PHI', 'captain': False}, {'player_id': 32122151, 'name': 'Nicolas Batum', 'salary': 6300, 'position': 'PF', 'status': '', 'fppg': 16.4, 'lock': False, 'exclude': False, 'min_exp': 0, 'max_exp': 100, 'team': 'PHI', 'captain': False}, {'player_id': 32122152, 'name': 'Marcus Morris Sr.', 'salary': 6000, 'position': 'SF/PF', 'status': '', 'fppg': 12.3, 'lock': False, 'exclude': False, 'min_exp': 0, 'max_exp': 100, 'team': 'PHI', 'captain': False}, {'player_id': 32122153, 'name': 'Patrick Beverley', 'salary': 5700, 'position': 'PG', 'status': '', 'fppg': 13.6, 'lock': False, 'exclude': False, 'min_exp': 0, 'max_exp': 100, 'team': 'PHI', 'captain': False}, {'player_id': 32122154, 'name': 'Terry Taylor', 'salary': 5400, 'position': 'C', 'status': '', 'fppg': 3.2, 'lock': False, 'exclude': False, 'min_exp': 0, 'max_exp': 100, 'team': 'CHI', 'captain': False},
{'player_id': 32122155, 'name': 'Robert Covington', 'salary': 4500, 'position': 'PF', 'status': '', 'fppg': 13.9, 'lock': False, 'exclude': False, 'min_exp': 0, 'max_exp': 100, 'team': 'PHI', 'captain': False}, {'player_id': 32122156, 'name': 'Jevon Carter', 'salary': 4200, 'position': 'PG/SG', 'status': '', 'fppg':
9.9, 'lock': False, 'exclude': False, 'min_exp': 0, 'max_exp': 100, 'team': 'CHI', 'captain': False}, {'player_id': 32122157, 'name': 'Kenneth Lofton Jr.', 'salary': 3900, 'position': 'C', 'status': '', 'fppg': 6.0, 'lock': False, 'exclude': False, 'min_exp': 0, 'max_exp': 100, 'team': 'PHI', 'captain': False}, {'player_id': 32122158, 'name': 'Dalen Terry', 'salary': 2700, 'position': 'PF', 'status': '', 'fppg': 6.0, 'lock': False, 'exclude': False, 'min_exp': 0, 'max_exp': 100, 'team': 'CHI', 'captain': False}, {'player_id': 32122159, 'name': 'Torrey Craig', 'salary': 1500, 'position': 'PF', 'status': '', 'fppg': 15.9, 'lock':
False, 'exclude': True, 'min_exp': 0, 'max_exp': 100, 'team': 'CHI', 'captain': False}, {'player_id': 32122162, 'name': 'KJ Martin', 'salary': 1500, 'position': 'PF', 'status': '', 'fppg': 4.5, 'lock': False, 'exclude': False, 'min_exp': 0, 'max_exp': 100, 'team': 'PHI', 'captain': False}, {'player_id': 32122163, 'name': 'Danuel House Jr.', 'salary': 1500, 'position': 'SF', 'status': '', 'fppg': 6.7, 'lock': False, 'exclude': False, 'min_exp': 0, 'max_exp': 100, 'team': 'PHI', 'captain': False}, {'player_id': 32122164, 'name': 'Furkan Korkmaz', 'salary': 1500, 'position': 'SG', 'status': '', 'fppg': 4.1, 'lock': False, 'exclude': False, 'min_exp': 0, 'max_exp': 100, 'team': 'PHI', 'captain': False}, {'player_id': 32122165, 'name': 'Adama Sanogo', 'salary': 1500, 'position': 'C', 'status': '', 'fppg': 0.0, 'lock': False, 'exclude': False, 'min_exp': 0, 'max_exp': 100, 'team': 'CHI', 'captain': False}, {'player_id': 32122166, 'name': 'Onuralp Bitim', 'salary': 1500, 'position': 'SG', 'status': '', 'fppg': 0.0, 'lock': False, 'exclude': True, 'min_exp': 0, 'max_exp': 100, 'team': 'CHI', 'captain': False}, {'player_id': 32122168, 'name': 'Julian Phillips', 'salary': 1500, 'position': 'PF', 'status': '', 'fppg': 1.8, 'lock': False, 'exclude': False, 'min_exp': 0, 'max_exp': 100, 'team': 'CHI', 'captain': False}, {'player_id': 32122170, 'name': 'Jaden Springer', 'salary': 1500, 'position': 'SG', 'status': '', 'fppg': 7.8, 'lock': False, 'exclude': False, 'min_exp': 0, 'max_exp': 100, 'team': 'PHI', 'captain': False}]

def capt_boost(p):
    return Player(
        name=p.name,
        pos=p.pos,
        cost=p.cost * 1.5,
        proj=p.proj * 1.5,
        possible_positions=p.possible_positions
    )


def add_capts(player_pool, captain):
    ###Make a pool with each player as a captain with 1.5x stats and make all players showdownplayers
    captain_pool = [capt_boost(p) for p in player_pool]
    pool = []
    for p in player_pool:
        pool.append(ShowdownPlayer(p))
    if captain:
        pool.append(ShowdownPlayer(captain[0], captain=True))
    else:
        for p in captain_pool:
            pool.append(ShowdownPlayer(p, captain=True))
    return pool


def get_player_pool(formset):
    """
    Input from PlayerFormSet
    """
    locks = []
    excluded_players = []
    exposure_bounds = []
    player_pool = []
    captain = []
    for form in formset:
        if form['captain'] == True:
            captain.append(Player(
                name=str(form['player_id']),
                cost=form['salary'],
                proj=form['fppg'],
                pos=pos,
                possible_positions=form['position'],
                lock=True
                   ))
        if form['exclude'] == True:
            excluded_players.append(str(form['player_id']))
        if form['lock']:
            locks.append(str(form['player_id']))
        if form['min_exp'] != 0 or form['max_exp'] != 100:
            min = float(form['min_exp'])/100
            max = float(form['max_exp'])/100
            exposure_bounds.append({
                'name':str(form['player_id']),
                'min': min,
                'max': max,
            })
        if '/' in form['position']:
            positions = form['position'].split('/')
            pos = positions[0]
        else:
            pos = form['position']

        player_pool.append(
            Player(
                name=str(form['player_id']),
                cost=form['salary'],
                proj=form['fppg'],
                pos=pos,
                possible_positions=form['position'],
                   )
            )
    ###Make Captain Pool
    pool = add_capts(player_pool, captain)

    constraints = LineupConstraints(
        locked=locks,
        banned=excluded_players,

    )
    rosters = run_multi(
        iterations=5,
        rule_set=rules.DK_NBA_SHOWDOWN_RULE_SET,#DK_NBA_RULE_SET
        player_pool=pool,
        verbose=False,
        constraints=constraints,
        exposure_bounds=exposure_bounds,
    )
    for roster in rosters[0]:
        ros = roster.sorted_players()
        print('-----')
        for player in ros:
            print(player.pos,player.name)


get_player_pool(formsets)
