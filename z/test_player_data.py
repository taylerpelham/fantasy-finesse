import http.client
import json
team = 1
def get_players_from_team(team):
    conn = http.client.HTTPSConnection("api-nba-v1.p.rapidapi.com")
    headers = {
        'X-RapidAPI-Key': "b99d40bbc6msh3aa3e31f97d65b8p1d29ddjsnbf5d620b0880",
        'X-RapidAPI-Host': "api-nba-v1.p.rapidapi.com"
    }
    conn.request("GET", f"/players?team={team}&season=2023", headers=headers)
    res = conn.getresponse()
    data = res.read()
    team_players_dict = json.loads(data.decode("utf-8"))
    return team_players_dict

def create_players_from_team_dict(team_players_dict, team):
    for response in team_players_dict['response']:
        print(response['id'])
        print(response['firstname'])
        print(response['lastname'])

team_id = 16

dict1 = get_players_from_team(team_id)

create_players_from_team_dict(dict1, team_id)
