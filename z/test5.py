from draftfast import rules
from draftfast.optimize import run, run_multi
from draftfast.orm import Player
from draftfast.csv_parse import salary_download
from draftfast.lineup_constraints import LineupConstraints
import csv
from os import environ
from draftfast import rules
from draftfast.optimize import run_multi
from draftfast.csv_parse import salary_download, uploaders

players = salary_download.generate_players_from_csvs(
  salary_file_location='DKSalaries (6).csv',
  game=rules.DRAFT_KINGS,
)
cons = LineupConstraints(
    locked=[]
)
expo_bounds = []
rosters,_ = run_multi(
      constraints = cons,
      iterations=20,
      rule_set=rules.DK_NBA_RULE_SET,
      player_pool=players,
      verbose=True,
      exposure_bounds=expo_bounds
  )
uploader = uploaders.DraftKingsNBAUploader(
  pid_file='DKSalaries (7).csv'
)
uploader.write_rosters(rosters)
