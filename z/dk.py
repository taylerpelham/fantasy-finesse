import requests
import json


"""
Currentlty a working way to get if player is starting
if player['playerGameAttributes'] returns a [{'id': 100, 'value': 'true'}]
then player is starting
"""

def get_starters(draft_pool_id=0):
    contests_response = requests.get(f'https://api.draftkings.com/draftgroups/v1/draftgroups/98608/draftables')
    contests_json = contests_response.json()
    for player in contests_json['draftables']:
        print(player['playerId'])
        print(player['playerGameAttributes'])
        print(player['status'])



def gamesets(draft_pool_id=0):
    contests_response = requests.get(f'https://api.draftkings.com/draftgroups/v1/draftgroups/98481/draftables')
    contests_json = contests_response.json()
    #print(contests_json['draftables'][:5])
    for player in contests_json['draftables']:
        print(player['shortName'])
        print(player['rosterSlotId'])
        print(player['salary'])


get_starters()
