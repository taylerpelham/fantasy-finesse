response = [
  {
    "market_label": "Rebounds",
    "player": {
      "name": "Kyle Kuzma",
      "position": "F",
      "team": "WAS"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 1,
            "bookie": "FanDuel",
            "line": {
              "cost": 1.98,
              "line": 6.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 2,
            "bookie": "DraftKings",
            "line": {
              "cost": 1.95,
              "line": 6.5
            }
          },
          {
            "id": 4,
            "bookie": "PointsBet",
            "line": {
              "cost": 1.95,
              "line": 6.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Blocks",
    "player": {
      "name": "Bilal Coulibaly",
      "position": "F",
      "team": "WAS"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 2.4,
              "line": 0.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 1.59,
              "line": 0.5
            }
          },
          {
            "id": 19,
            "bookie": "Barstool",
            "line": {
              "cost": 1.59,
              "line": 0.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Points",
    "player": {
      "name": "Deni Avdija",
      "position": "F",
      "team": "WAS"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 1,
            "bookie": "FanDuel",
            "line": {
              "cost": 1.83,
              "line": 11.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 8,
            "bookie": "BetMGM",
            "line": {
              "cost": 1.83,
              "line": 12.5
            }
          },
          {
            "id": 13,
            "bookie": "PartyCasino",
            "line": {
              "cost": 1.83,
              "line": 12.5
            }
          },
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 1.83,
              "line": 12.5
            }
          },
          {
            "id": 19,
            "bookie": "Barstool",
            "line": {
              "cost": 1.83,
              "line": 12.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Points",
    "player": {
      "name": "Daniel Gafford",
      "position": "F",
      "team": "WAS"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 1,
            "bookie": "FanDuel",
            "line": {
              "cost": 1.85,
              "line": 10.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 2,
            "bookie": "DraftKings",
            "line": {
              "cost": 1.8,
              "line": 11.5
            }
          },
          {
            "id": 4,
            "bookie": "PointsBet",
            "line": {
              "cost": 1.8,
              "line": 11.5
            }
          },
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 1.8,
              "line": 11.5
            }
          },
          {
            "id": 19,
            "bookie": "Barstool",
            "line": {
              "cost": 1.8,
              "line": 11.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Assists",
    "player": {
      "name": "Caris LeVert",
      "position": "G",
      "team": "CLE"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 2,
            "bookie": "DraftKings",
            "line": {
              "cost": 1.95,
              "line": 4.5
            }
          },
          {
            "id": 8,
            "bookie": "BetMGM",
            "line": {
              "cost": 1.95,
              "line": 4.5
            }
          },
          {
            "id": 13,
            "bookie": "PartyCasino",
            "line": {
              "cost": 1.95,
              "line": 4.5
            }
          },
          {
            "id": 19,
            "bookie": "Barstool",
            "line": {
              "cost": 1.95,
              "line": 4.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 4,
            "bookie": "PointsBet",
            "line": {
              "cost": 1.83,
              "line": 4.5
            }
          },
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 1.83,
              "line": 4.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Steals",
    "player": {
      "name": "Deni Avdija",
      "position": "F",
      "team": "WAS"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 4,
            "bookie": "PointsBet",
            "line": {
              "cost": 1.77,
              "line": 0.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 8,
            "bookie": "BetMGM",
            "line": {
              "cost": 2.1,
              "line": 0.5
            }
          },
          {
            "id": 3,
            "bookie": "Caesars",
            "line": {
              "cost": 2.1,
              "line": 0.5
            }
          },
          {
            "id": 13,
            "bookie": "PartyCasino",
            "line": {
              "cost": 2.1,
              "line": 0.5
            }
          },
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 2.1,
              "line": 0.5
            }
          },
          {
            "id": 19,
            "bookie": "Barstool",
            "line": {
              "cost": 2.1,
              "line": 0.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Points",
    "player": {
      "name": "Max Strus",
      "position": "G",
      "team": "CLE"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 1,
            "bookie": "FanDuel",
            "line": {
              "cost": 1.94,
              "line": 15.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 4,
            "bookie": "PointsBet",
            "line": {
              "cost": 1.83,
              "line": 16.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Blocks",
    "player": {
      "name": "Donovan Mitchell",
      "position": "G",
      "team": "CLE"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 2.55,
              "line": 0.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 1.53,
              "line": 0.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Blocks",
    "player": {
      "name": "Jordan Poole",
      "position": "G",
      "team": "WAS"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 3.2,
              "line": 0.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 4,
            "bookie": "PointsBet",
            "line": {
              "cost": 1.36,
              "line": 0.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Steals",
    "player": {
      "name": "Jarrett Allen",
      "position": "C",
      "team": "CLE"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 4,
            "bookie": "PointsBet",
            "line": {
              "cost": 1.59,
              "line": 0.5
            }
          },
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 1.59,
              "line": 0.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 2.4,
              "line": 0.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Three Points Made",
    "player": {
      "name": "Corey Kispert",
      "position": "F",
      "team": "WAS"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 8,
            "bookie": "BetMGM",
            "line": {
              "cost": 1.61,
              "line": 1.5
            }
          },
          {
            "id": 13,
            "bookie": "PartyCasino",
            "line": {
              "cost": 1.61,
              "line": 1.5
            }
          },
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 1.61,
              "line": 1.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 2.35,
              "line": 1.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Rebounds",
    "player": {
      "name": "Daniel Gafford",
      "position": "F",
      "team": "WAS"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 1,
            "bookie": "FanDuel",
            "line": {
              "cost": 2.02,
              "line": 7.5
            }
          },
          {
            "id": 5,
            "bookie": "SugarHouse",
            "line": {
              "cost": 2.02,
              "line": 7.5
            }
          },
          {
            "id": 7,
            "bookie": "BetRivers",
            "line": {
              "cost": 2.02,
              "line": 7.5
            }
          },
          {
            "id": 14,
            "bookie": "UniBet",
            "line": {
              "cost": 2.02,
              "line": 7.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 1.83,
              "line": 7.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Blocks",
    "player": {
      "name": "Jarrett Allen",
      "position": "C",
      "team": "CLE"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 2.8,
              "line": 1.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 1,
            "bookie": "FanDuel",
            "line": {
              "cost": 1.47,
              "line": 1.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Points",
    "player": {
      "name": "Isaac Okoro",
      "position": "F",
      "team": "CLE"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 1,
            "bookie": "FanDuel",
            "line": {
              "cost": 1.98,
              "line": 11.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 3,
            "bookie": "Caesars",
            "line": {
              "cost": 1.92,
              "line": 11.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Rebounds",
    "player": {
      "name": "Corey Kispert",
      "position": "F",
      "team": "WAS"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 1.69,
              "line": 1.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 8,
            "bookie": "BetMGM",
            "line": {
              "cost": 2.15,
              "line": 1.5
            }
          },
          {
            "id": 13,
            "bookie": "PartyCasino",
            "line": {
              "cost": 2.15,
              "line": 1.5
            }
          },
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 2.15,
              "line": 1.5
            }
          },
          {
            "id": 19,
            "bookie": "Barstool",
            "line": {
              "cost": 2.15,
              "line": 1.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Assists",
    "player": {
      "name": "Jordan Poole",
      "position": "G",
      "team": "WAS"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 1,
            "bookie": "FanDuel",
            "line": {
              "cost": 1.78,
              "line": 3.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 5,
            "bookie": "SugarHouse",
            "line": {
              "cost": 2.2,
              "line": 3.5
            }
          },
          {
            "id": 7,
            "bookie": "BetRivers",
            "line": {
              "cost": 2.2,
              "line": 3.5
            }
          },
          {
            "id": 14,
            "bookie": "UniBet",
            "line": {
              "cost": 2.2,
              "line": 3.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Rebounds",
    "player": {
      "name": "Donovan Mitchell",
      "position": "G",
      "team": "CLE"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 1,
            "bookie": "FanDuel",
            "line": {
              "cost": 1.81,
              "line": 5.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 3,
            "bookie": "Caesars",
            "line": {
              "cost": 1.68,
              "line": 6.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Assists",
    "player": {
      "name": "Georges Niang",
      "position": "F",
      "team": "CLE"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 1.44,
              "line": 0.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 19,
            "bookie": "Barstool",
            "line": {
              "cost": 1.42,
              "line": 1.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Three Points Made",
    "player": {
      "name": "Jordan Poole",
      "position": "G",
      "team": "WAS"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 2.35,
              "line": 2.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 1,
            "bookie": "FanDuel",
            "line": {
              "cost": 1.64,
              "line": 2.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Points",
    "player": {
      "name": "Tyus Jones",
      "position": "G",
      "team": "WAS"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 5,
            "bookie": "SugarHouse",
            "line": {
              "cost": 1.75,
              "line": 11.5
            }
          },
          {
            "id": 7,
            "bookie": "BetRivers",
            "line": {
              "cost": 1.75,
              "line": 11.5
            }
          },
          {
            "id": 14,
            "bookie": "UniBet",
            "line": {
              "cost": 1.75,
              "line": 11.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 2,
            "bookie": "DraftKings",
            "line": {
              "cost": 1.83,
              "line": 12.5
            }
          },
          {
            "id": 8,
            "bookie": "BetMGM",
            "line": {
              "cost": 1.83,
              "line": 12.5
            }
          },
          {
            "id": 13,
            "bookie": "PartyCasino",
            "line": {
              "cost": 1.83,
              "line": 12.5
            }
          },
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 1.83,
              "line": 12.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Three Points Made",
    "player": {
      "name": "Caris LeVert",
      "position": "G",
      "team": "CLE"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 1.65,
              "line": 1.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 2.25,
              "line": 1.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Assists",
    "player": {
      "name": "Isaac Okoro",
      "position": "F",
      "team": "CLE"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 2,
            "bookie": "DraftKings",
            "line": {
              "cost": 1.74,
              "line": 2.5
            }
          },
          {
            "id": 19,
            "bookie": "Barstool",
            "line": {
              "cost": 1.74,
              "line": 2.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 8,
            "bookie": "BetMGM",
            "line": {
              "cost": 2.1,
              "line": 2.5
            }
          },
          {
            "id": 13,
            "bookie": "PartyCasino",
            "line": {
              "cost": 2.1,
              "line": 2.5
            }
          },
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 2.1,
              "line": 2.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Assists",
    "player": {
      "name": "Daniel Gafford",
      "position": "F",
      "team": "WAS"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 19,
            "bookie": "Barstool",
            "line": {
              "cost": 1.25,
              "line": 0.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 8,
            "bookie": "BetMGM",
            "line": {
              "cost": 1.65,
              "line": 1.5
            }
          },
          {
            "id": 13,
            "bookie": "PartyCasino",
            "line": {
              "cost": 1.65,
              "line": 1.5
            }
          },
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 1.65,
              "line": 1.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Blocks",
    "player": {
      "name": "Corey Kispert",
      "position": "F",
      "team": "WAS"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 8,
            "bookie": "BetMGM",
            "line": {
              "cost": 5.25,
              "line": 0.5
            }
          },
          {
            "id": 13,
            "bookie": "PartyCasino",
            "line": {
              "cost": 5.25,
              "line": 0.5
            }
          },
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 5.25,
              "line": 0.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 4,
            "bookie": "PointsBet",
            "line": {
              "cost": 1.16,
              "line": 0.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Three Points Made",
    "player": {
      "name": "Max Strus",
      "position": "G",
      "team": "CLE"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 2.35,
              "line": 3.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 5,
            "bookie": "SugarHouse",
            "line": {
              "cost": 1.63,
              "line": 3.5
            }
          },
          {
            "id": 7,
            "bookie": "BetRivers",
            "line": {
              "cost": 1.63,
              "line": 3.5
            }
          },
          {
            "id": 14,
            "bookie": "UniBet",
            "line": {
              "cost": 1.63,
              "line": 3.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Rebounds",
    "player": {
      "name": "Caris LeVert",
      "position": "G",
      "team": "CLE"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 1.63,
              "line": 3.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 19,
            "bookie": "Barstool",
            "line": {
              "cost": 1.59,
              "line": 4.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Rebounds",
    "player": {
      "name": "Isaac Okoro",
      "position": "F",
      "team": "CLE"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 1,
            "bookie": "FanDuel",
            "line": {
              "cost": 1.78,
              "line": 3.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 2,
            "bookie": "DraftKings",
            "line": {
              "cost": 2.05,
              "line": 3.5
            }
          },
          {
            "id": 8,
            "bookie": "BetMGM",
            "line": {
              "cost": 2.05,
              "line": 3.5
            }
          },
          {
            "id": 4,
            "bookie": "PointsBet",
            "line": {
              "cost": 2.05,
              "line": 3.5
            }
          },
          {
            "id": 13,
            "bookie": "PartyCasino",
            "line": {
              "cost": 2.05,
              "line": 3.5
            }
          },
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 2.05,
              "line": 3.5
            }
          },
          {
            "id": 19,
            "bookie": "Barstool",
            "line": {
              "cost": 2.05,
              "line": 3.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Rebounds",
    "player": {
      "name": "Jarrett Allen",
      "position": "C",
      "team": "CLE"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 1,
            "bookie": "FanDuel",
            "line": {
              "cost": 1.72,
              "line": 11.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 4,
            "bookie": "PointsBet",
            "line": {
              "cost": 1.87,
              "line": 12.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Three Points Made",
    "player": {
      "name": "Tyus Jones",
      "position": "G",
      "team": "WAS"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 5,
            "bookie": "SugarHouse",
            "line": {
              "cost": 2,
              "line": 1.5
            }
          },
          {
            "id": 7,
            "bookie": "BetRivers",
            "line": {
              "cost": 2,
              "line": 1.5
            }
          },
          {
            "id": 14,
            "bookie": "UniBet",
            "line": {
              "cost": 2,
              "line": 1.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 1.87,
              "line": 1.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Rebounds",
    "player": {
      "name": "Bilal Coulibaly",
      "position": "F",
      "team": "WAS"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 2,
            "bookie": "DraftKings",
            "line": {
              "cost": 1.95,
              "line": 3.5
            }
          },
          {
            "id": 8,
            "bookie": "BetMGM",
            "line": {
              "cost": 1.95,
              "line": 3.5
            }
          },
          {
            "id": 13,
            "bookie": "PartyCasino",
            "line": {
              "cost": 1.95,
              "line": 3.5
            }
          },
          {
            "id": 19,
            "bookie": "Barstool",
            "line": {
              "cost": 1.95,
              "line": 3.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 1.83,
              "line": 3.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Blocks",
    "player": {
      "name": "Georges Niang",
      "position": "F",
      "team": "CLE"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 6.25,
              "line": 0.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 4,
            "bookie": "PointsBet",
            "line": {
              "cost": 1.13,
              "line": 0.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Points",
    "player": {
      "name": "Georges Niang",
      "position": "F",
      "team": "CLE"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 2,
            "bookie": "DraftKings",
            "line": {
              "cost": 1.95,
              "line": 7.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 1.87,
              "line": 7.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Three Points Made",
    "player": {
      "name": "Isaac Okoro",
      "position": "F",
      "team": "CLE"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 5,
            "bookie": "SugarHouse",
            "line": {
              "cost": 2.65,
              "line": 1.5
            }
          },
          {
            "id": 7,
            "bookie": "BetRivers",
            "line": {
              "cost": 2.65,
              "line": 1.5
            }
          },
          {
            "id": 14,
            "bookie": "UniBet",
            "line": {
              "cost": 2.65,
              "line": 1.5
            }
          },
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 2.65,
              "line": 1.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 3,
            "bookie": "Caesars",
            "line": {
              "cost": 1.49,
              "line": 1.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Steals",
    "player": {
      "name": "Dean Wade",
      "position": "F",
      "team": "CLE"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 4,
            "bookie": "PointsBet",
            "line": {
              "cost": 1.59,
              "line": 0.5
            }
          },
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 1.59,
              "line": 0.5
            }
          },
          {
            "id": 19,
            "bookie": "Barstool",
            "line": {
              "cost": 1.59,
              "line": 0.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 2.4,
              "line": 0.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Rebounds",
    "player": {
      "name": "Max Strus",
      "position": "G",
      "team": "CLE"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 1,
            "bookie": "FanDuel",
            "line": {
              "cost": 1.81,
              "line": 4.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 2,
            "bookie": "DraftKings",
            "line": {
              "cost": 2.2,
              "line": 4.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Steals",
    "player": {
      "name": "Daniel Gafford",
      "position": "F",
      "team": "WAS"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 19,
            "bookie": "Barstool",
            "line": {
              "cost": 1.71,
              "line": 0.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 2.15,
              "line": 0.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Rebounds",
    "player": {
      "name": "Dean Wade",
      "position": "F",
      "team": "CLE"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 1,
            "bookie": "FanDuel",
            "line": {
              "cost": 2.2,
              "line": 5.5
            }
          },
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 2.2,
              "line": 5.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 2,
            "bookie": "DraftKings",
            "line": {
              "cost": 1.71,
              "line": 5.5
            }
          },
          {
            "id": 4,
            "bookie": "PointsBet",
            "line": {
              "cost": 1.71,
              "line": 5.5
            }
          },
          {
            "id": 19,
            "bookie": "Barstool",
            "line": {
              "cost": 1.71,
              "line": 5.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Rebounds",
    "player": {
      "name": "Delon Wright",
      "position": "G",
      "team": "WAS"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 8,
            "bookie": "BetMGM",
            "line": {
              "cost": 1.95,
              "line": 2.5
            }
          },
          {
            "id": 13,
            "bookie": "PartyCasino",
            "line": {
              "cost": 1.95,
              "line": 2.5
            }
          },
          {
            "id": 19,
            "bookie": "Barstool",
            "line": {
              "cost": 1.95,
              "line": 2.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 2,
            "bookie": "DraftKings",
            "line": {
              "cost": 1.83,
              "line": 2.5
            }
          },
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 1.83,
              "line": 2.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Steals",
    "player": {
      "name": "Max Strus",
      "position": "G",
      "team": "CLE"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 4,
            "bookie": "PointsBet",
            "line": {
              "cost": 1.48,
              "line": 0.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 1,
            "bookie": "FanDuel",
            "line": {
              "cost": 2.72,
              "line": 0.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Steals",
    "player": {
      "name": "Caris LeVert",
      "position": "G",
      "team": "CLE"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 4,
            "bookie": "PointsBet",
            "line": {
              "cost": 1.59,
              "line": 0.5
            }
          },
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 1.59,
              "line": 0.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 2.4,
              "line": 0.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Assists",
    "player": {
      "name": "Tyus Jones",
      "position": "G",
      "team": "WAS"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 1,
            "bookie": "FanDuel",
            "line": {
              "cost": 1.7,
              "line": 5.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 5,
            "bookie": "SugarHouse",
            "line": {
              "cost": 1.68,
              "line": 6.5
            }
          },
          {
            "id": 7,
            "bookie": "BetRivers",
            "line": {
              "cost": 1.68,
              "line": 6.5
            }
          },
          {
            "id": 14,
            "bookie": "UniBet",
            "line": {
              "cost": 1.68,
              "line": 6.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Three Points Made",
    "player": {
      "name": "Georges Niang",
      "position": "F",
      "team": "CLE"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 2,
            "bookie": "DraftKings",
            "line": {
              "cost": 1.95,
              "line": 1.5
            }
          },
          {
            "id": 8,
            "bookie": "BetMGM",
            "line": {
              "cost": 1.95,
              "line": 1.5
            }
          },
          {
            "id": 13,
            "bookie": "PartyCasino",
            "line": {
              "cost": 1.95,
              "line": 1.5
            }
          },
          {
            "id": 19,
            "bookie": "Barstool",
            "line": {
              "cost": 1.95,
              "line": 1.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 1.83,
              "line": 1.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Blocks",
    "player": {
      "name": "Max Strus",
      "position": "G",
      "team": "CLE"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 2.65,
              "line": 0.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 4,
            "bookie": "PointsBet",
            "line": {
              "cost": 1.5,
              "line": 0.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Assists",
    "player": {
      "name": "Delon Wright",
      "position": "G",
      "team": "WAS"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 1.8,
              "line": 2.5
            }
          },
          {
            "id": 19,
            "bookie": "Barstool",
            "line": {
              "cost": 1.8,
              "line": 2.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 2,
            "bookie": "DraftKings",
            "line": {
              "cost": 2,
              "line": 2.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Blocks",
    "player": {
      "name": "Delon Wright",
      "position": "G",
      "team": "WAS"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 4.2,
              "line": 0.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 8,
            "bookie": "BetMGM",
            "line": {
              "cost": 1.22,
              "line": 0.5
            }
          },
          {
            "id": 13,
            "bookie": "PartyCasino",
            "line": {
              "cost": 1.22,
              "line": 0.5
            }
          },
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 1.22,
              "line": 0.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Steals",
    "player": {
      "name": "Kyle Kuzma",
      "position": "F",
      "team": "WAS"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 2.35,
              "line": 0.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 8,
            "bookie": "BetMGM",
            "line": {
              "cost": 1.61,
              "line": 0.5
            }
          },
          {
            "id": 13,
            "bookie": "PartyCasino",
            "line": {
              "cost": 1.61,
              "line": 0.5
            }
          },
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 1.61,
              "line": 0.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Blocks",
    "player": {
      "name": "Deni Avdija",
      "position": "F",
      "team": "WAS"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 2.55,
              "line": 0.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 4,
            "bookie": "PointsBet",
            "line": {
              "cost": 1.53,
              "line": 0.5
            }
          },
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 1.53,
              "line": 0.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Points",
    "player": {
      "name": "Jarrett Allen",
      "position": "C",
      "team": "CLE"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 1,
            "bookie": "FanDuel",
            "line": {
              "cost": 1.87,
              "line": 18.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 5,
            "bookie": "SugarHouse",
            "line": {
              "cost": 1.87,
              "line": 19.5
            }
          },
          {
            "id": 7,
            "bookie": "BetRivers",
            "line": {
              "cost": 1.87,
              "line": 19.5
            }
          },
          {
            "id": 14,
            "bookie": "UniBet",
            "line": {
              "cost": 1.87,
              "line": 19.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Steals",
    "player": {
      "name": "Donovan Mitchell",
      "position": "G",
      "team": "CLE"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 4,
            "bookie": "PointsBet",
            "line": {
              "cost": 1.91,
              "line": 1.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 1,
            "bookie": "FanDuel",
            "line": {
              "cost": 2,
              "line": 1.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Assists",
    "player": {
      "name": "Donovan Mitchell",
      "position": "G",
      "team": "CLE"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 1,
            "bookie": "FanDuel",
            "line": {
              "cost": 2.12,
              "line": 7.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 4,
            "bookie": "PointsBet",
            "line": {
              "cost": 1.83,
              "line": 7.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Assists",
    "player": {
      "name": "Kyle Kuzma",
      "position": "F",
      "team": "WAS"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 1,
            "bookie": "FanDuel",
            "line": {
              "cost": 1.83,
              "line": 3.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 2,
            "bookie": "DraftKings",
            "line": {
              "cost": 2.1,
              "line": 3.5
            }
          },
          {
            "id": 8,
            "bookie": "BetMGM",
            "line": {
              "cost": 2.1,
              "line": 3.5
            }
          },
          {
            "id": 4,
            "bookie": "PointsBet",
            "line": {
              "cost": 2.1,
              "line": 3.5
            }
          },
          {
            "id": 13,
            "bookie": "PartyCasino",
            "line": {
              "cost": 2.1,
              "line": 3.5
            }
          },
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 2.1,
              "line": 3.5
            }
          },
          {
            "id": 19,
            "bookie": "Barstool",
            "line": {
              "cost": 2.1,
              "line": 3.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Assists",
    "player": {
      "name": "Deni Avdija",
      "position": "F",
      "team": "WAS"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 1,
            "bookie": "FanDuel",
            "line": {
              "cost": 2.1,
              "line": 3.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 1.87,
              "line": 3.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Three Points Made",
    "player": {
      "name": "Delon Wright",
      "position": "G",
      "team": "WAS"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 2.4,
              "line": 0.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 1.59,
              "line": 0.5
            }
          },
          {
            "id": 19,
            "bookie": "Barstool",
            "line": {
              "cost": 1.59,
              "line": 0.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Assists",
    "player": {
      "name": "Dean Wade",
      "position": "F",
      "team": "CLE"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 1.42,
              "line": 0.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 19,
            "bookie": "Barstool",
            "line": {
              "cost": 1.09,
              "line": 2.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Blocks",
    "player": {
      "name": "Kyle Kuzma",
      "position": "F",
      "team": "WAS"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 2.45,
              "line": 0.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 4,
            "bookie": "PointsBet",
            "line": {
              "cost": 1.56,
              "line": 0.5
            }
          },
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 1.56,
              "line": 0.5
            }
          },
          {
            "id": 19,
            "bookie": "Barstool",
            "line": {
              "cost": 1.56,
              "line": 0.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Points",
    "player": {
      "name": "Bilal Coulibaly",
      "position": "F",
      "team": "WAS"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 8,
            "bookie": "BetMGM",
            "line": {
              "cost": 1.87,
              "line": 5.5
            }
          },
          {
            "id": 13,
            "bookie": "PartyCasino",
            "line": {
              "cost": 1.87,
              "line": 5.5
            }
          },
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 1.87,
              "line": 5.5
            }
          },
          {
            "id": 19,
            "bookie": "Barstool",
            "line": {
              "cost": 1.87,
              "line": 5.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 8,
            "bookie": "BetMGM",
            "line": {
              "cost": 1.87,
              "line": 5.5
            }
          },
          {
            "id": 13,
            "bookie": "PartyCasino",
            "line": {
              "cost": 1.87,
              "line": 5.5
            }
          },
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 1.87,
              "line": 5.5
            }
          },
          {
            "id": 19,
            "bookie": "Barstool",
            "line": {
              "cost": 1.87,
              "line": 5.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Three Points Made",
    "player": {
      "name": "Kyle Kuzma",
      "position": "F",
      "team": "WAS"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 1,
            "bookie": "FanDuel",
            "line": {
              "cost": 1.76,
              "line": 2.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 19,
            "bookie": "Barstool",
            "line": {
              "cost": 2.2,
              "line": 2.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Steals",
    "player": {
      "name": "Delon Wright",
      "position": "G",
      "team": "WAS"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 1.65,
              "line": 0.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 2.25,
              "line": 0.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Steals",
    "player": {
      "name": "Tyus Jones",
      "position": "G",
      "team": "WAS"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 3,
            "bookie": "Caesars",
            "line": {
              "cost": 1.41,
              "line": 0.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 1.41,
              "line": 1.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Points",
    "player": {
      "name": "Caris LeVert",
      "position": "G",
      "team": "CLE"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 4,
            "bookie": "PointsBet",
            "line": {
              "cost": 1.8,
              "line": 15.5
            }
          },
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 1.8,
              "line": 15.5
            }
          },
          {
            "id": 19,
            "bookie": "Barstool",
            "line": {
              "cost": 1.8,
              "line": 15.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 2,
            "bookie": "DraftKings",
            "line": {
              "cost": 2,
              "line": 15.5
            }
          },
          {
            "id": 8,
            "bookie": "BetMGM",
            "line": {
              "cost": 2,
              "line": 15.5
            }
          },
          {
            "id": 13,
            "bookie": "PartyCasino",
            "line": {
              "cost": 2,
              "line": 15.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Steals",
    "player": {
      "name": "Jordan Poole",
      "position": "G",
      "team": "WAS"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 1.69,
              "line": 0.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 4,
            "bookie": "PointsBet",
            "line": {
              "cost": 2.15,
              "line": 0.5
            }
          },
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 2.15,
              "line": 0.5
            }
          },
          {
            "id": 19,
            "bookie": "Barstool",
            "line": {
              "cost": 2.15,
              "line": 0.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Blocks",
    "player": {
      "name": "Daniel Gafford",
      "position": "F",
      "team": "WAS"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 4,
            "bookie": "PointsBet",
            "line": {
              "cost": 1.61,
              "line": 1.5
            }
          },
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 1.61,
              "line": 1.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 1,
            "bookie": "FanDuel",
            "line": {
              "cost": 2.4,
              "line": 1.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Points",
    "player": {
      "name": "Delon Wright",
      "position": "G",
      "team": "WAS"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 8,
            "bookie": "BetMGM",
            "line": {
              "cost": 1.87,
              "line": 4.5
            }
          },
          {
            "id": 13,
            "bookie": "PartyCasino",
            "line": {
              "cost": 1.87,
              "line": 4.5
            }
          },
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 1.87,
              "line": 4.5
            }
          },
          {
            "id": 19,
            "bookie": "Barstool",
            "line": {
              "cost": 1.87,
              "line": 4.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 8,
            "bookie": "BetMGM",
            "line": {
              "cost": 1.87,
              "line": 4.5
            }
          },
          {
            "id": 13,
            "bookie": "PartyCasino",
            "line": {
              "cost": 1.87,
              "line": 4.5
            }
          },
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 1.87,
              "line": 4.5
            }
          },
          {
            "id": 19,
            "bookie": "Barstool",
            "line": {
              "cost": 1.87,
              "line": 4.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Rebounds",
    "player": {
      "name": "Deni Avdija",
      "position": "F",
      "team": "WAS"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 1,
            "bookie": "FanDuel",
            "line": {
              "cost": 2.16,
              "line": 6.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 2,
            "bookie": "DraftKings",
            "line": {
              "cost": 1.8,
              "line": 6.5
            }
          },
          {
            "id": 8,
            "bookie": "BetMGM",
            "line": {
              "cost": 1.8,
              "line": 6.5
            }
          },
          {
            "id": 4,
            "bookie": "PointsBet",
            "line": {
              "cost": 1.8,
              "line": 6.5
            }
          },
          {
            "id": 13,
            "bookie": "PartyCasino",
            "line": {
              "cost": 1.8,
              "line": 6.5
            }
          },
          {
            "id": 19,
            "bookie": "Barstool",
            "line": {
              "cost": 1.8,
              "line": 6.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Three Points Made",
    "player": {
      "name": "Bilal Coulibaly",
      "position": "F",
      "team": "WAS"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 1.83,
              "line": 0.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 2,
            "bookie": "DraftKings",
            "line": {
              "cost": 1.95,
              "line": 0.5
            }
          },
          {
            "id": 8,
            "bookie": "BetMGM",
            "line": {
              "cost": 1.95,
              "line": 0.5
            }
          },
          {
            "id": 13,
            "bookie": "PartyCasino",
            "line": {
              "cost": 1.95,
              "line": 0.5
            }
          },
          {
            "id": 19,
            "bookie": "Barstool",
            "line": {
              "cost": 1.95,
              "line": 0.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Blocks",
    "player": {
      "name": "Isaac Okoro",
      "position": "F",
      "team": "CLE"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 3,
            "bookie": "Caesars",
            "line": {
              "cost": 2.16,
              "line": 0.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 19,
            "bookie": "Barstool",
            "line": {
              "cost": 1.71,
              "line": 0.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Rebounds",
    "player": {
      "name": "Tyus Jones",
      "position": "G",
      "team": "WAS"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 1,
            "bookie": "FanDuel",
            "line": {
              "cost": 1.72,
              "line": 2.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 2.25,
              "line": 2.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Assists",
    "player": {
      "name": "Bilal Coulibaly",
      "position": "F",
      "team": "WAS"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 1.87,
              "line": 1.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 8,
            "bookie": "BetMGM",
            "line": {
              "cost": 1.91,
              "line": 1.5
            }
          },
          {
            "id": 13,
            "bookie": "PartyCasino",
            "line": {
              "cost": 1.91,
              "line": 1.5
            }
          },
          {
            "id": 19,
            "bookie": "Barstool",
            "line": {
              "cost": 1.91,
              "line": 1.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Three Points Made",
    "player": {
      "name": "Donovan Mitchell",
      "position": "G",
      "team": "CLE"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 1,
            "bookie": "FanDuel",
            "line": {
              "cost": 1.96,
              "line": 3.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 5,
            "bookie": "SugarHouse",
            "line": {
              "cost": 1.87,
              "line": 3.5
            }
          },
          {
            "id": 7,
            "bookie": "BetRivers",
            "line": {
              "cost": 1.87,
              "line": 3.5
            }
          },
          {
            "id": 14,
            "bookie": "UniBet",
            "line": {
              "cost": 1.87,
              "line": 3.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Points",
    "player": {
      "name": "Dean Wade",
      "position": "F",
      "team": "CLE"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 1,
            "bookie": "FanDuel",
            "line": {
              "cost": 1.71,
              "line": 5.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 4,
            "bookie": "PointsBet",
            "line": {
              "cost": 1.8,
              "line": 6.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Blocks",
    "player": {
      "name": "Caris LeVert",
      "position": "G",
      "team": "CLE"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 3.1,
              "line": 0.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 4,
            "bookie": "PointsBet",
            "line": {
              "cost": 1.38,
              "line": 0.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Steals",
    "player": {
      "name": "Georges Niang",
      "position": "F",
      "team": "CLE"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 2.8,
              "line": 0.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 4,
            "bookie": "PointsBet",
            "line": {
              "cost": 1.44,
              "line": 0.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Assists",
    "player": {
      "name": "Max Strus",
      "position": "G",
      "team": "CLE"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 1,
            "bookie": "FanDuel",
            "line": {
              "cost": 1.71,
              "line": 3.5
            }
          },
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 1.71,
              "line": 3.5
            }
          },
          {
            "id": 19,
            "bookie": "Barstool",
            "line": {
              "cost": 1.71,
              "line": 3.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 5,
            "bookie": "SugarHouse",
            "line": {
              "cost": 2.18,
              "line": 3.5
            }
          },
          {
            "id": 7,
            "bookie": "BetRivers",
            "line": {
              "cost": 2.18,
              "line": 3.5
            }
          },
          {
            "id": 14,
            "bookie": "UniBet",
            "line": {
              "cost": 2.18,
              "line": 3.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Points",
    "player": {
      "name": "Corey Kispert",
      "position": "F",
      "team": "WAS"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 2,
            "bookie": "DraftKings",
            "line": {
              "cost": 1.87,
              "line": 10.5
            }
          },
          {
            "id": 8,
            "bookie": "BetMGM",
            "line": {
              "cost": 1.87,
              "line": 10.5
            }
          },
          {
            "id": 4,
            "bookie": "PointsBet",
            "line": {
              "cost": 1.87,
              "line": 10.5
            }
          },
          {
            "id": 13,
            "bookie": "PartyCasino",
            "line": {
              "cost": 1.87,
              "line": 10.5
            }
          },
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 1.87,
              "line": 10.5
            }
          },
          {
            "id": 19,
            "bookie": "Barstool",
            "line": {
              "cost": 1.87,
              "line": 10.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 2,
            "bookie": "DraftKings",
            "line": {
              "cost": 1.87,
              "line": 10.5
            }
          },
          {
            "id": 8,
            "bookie": "BetMGM",
            "line": {
              "cost": 1.87,
              "line": 10.5
            }
          },
          {
            "id": 4,
            "bookie": "PointsBet",
            "line": {
              "cost": 1.87,
              "line": 10.5
            }
          },
          {
            "id": 13,
            "bookie": "PartyCasino",
            "line": {
              "cost": 1.87,
              "line": 10.5
            }
          },
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 1.87,
              "line": 10.5
            }
          },
          {
            "id": 19,
            "bookie": "Barstool",
            "line": {
              "cost": 1.87,
              "line": 10.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Steals",
    "player": {
      "name": "Isaac Okoro",
      "position": "F",
      "team": "CLE"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 4,
            "bookie": "PointsBet",
            "line": {
              "cost": 1.56,
              "line": 0.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 2.65,
              "line": 0.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Points",
    "player": {
      "name": "Jordan Poole",
      "position": "G",
      "team": "WAS"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 2,
            "bookie": "DraftKings",
            "line": {
              "cost": 1.87,
              "line": 16.5
            }
          },
          {
            "id": 1,
            "bookie": "FanDuel",
            "line": {
              "cost": 1.87,
              "line": 16.5
            }
          },
          {
            "id": 4,
            "bookie": "PointsBet",
            "line": {
              "cost": 1.87,
              "line": 16.5
            }
          },
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 1.87,
              "line": 16.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 5,
            "bookie": "SugarHouse",
            "line": {
              "cost": 1.83,
              "line": 17.5
            }
          },
          {
            "id": 7,
            "bookie": "BetRivers",
            "line": {
              "cost": 1.83,
              "line": 17.5
            }
          },
          {
            "id": 14,
            "bookie": "UniBet",
            "line": {
              "cost": 1.83,
              "line": 17.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Rebounds",
    "player": {
      "name": "Georges Niang",
      "position": "F",
      "team": "CLE"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 2,
            "bookie": "DraftKings",
            "line": {
              "cost": 1.95,
              "line": 2.5
            }
          },
          {
            "id": 8,
            "bookie": "BetMGM",
            "line": {
              "cost": 1.95,
              "line": 2.5
            }
          },
          {
            "id": 13,
            "bookie": "PartyCasino",
            "line": {
              "cost": 1.95,
              "line": 2.5
            }
          },
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 1.95,
              "line": 2.5
            }
          },
          {
            "id": 19,
            "bookie": "Barstool",
            "line": {
              "cost": 1.95,
              "line": 2.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 4,
            "bookie": "PointsBet",
            "line": {
              "cost": 1.83,
              "line": 2.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Assists",
    "player": {
      "name": "Corey Kispert",
      "position": "F",
      "team": "WAS"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 2.6,
              "line": 1.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 8,
            "bookie": "BetMGM",
            "line": {
              "cost": 1.5,
              "line": 1.5
            }
          },
          {
            "id": 13,
            "bookie": "PartyCasino",
            "line": {
              "cost": 1.5,
              "line": 1.5
            }
          },
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 1.5,
              "line": 1.5
            }
          },
          {
            "id": 19,
            "bookie": "Barstool",
            "line": {
              "cost": 1.5,
              "line": 1.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Three Points Made",
    "player": {
      "name": "Dean Wade",
      "position": "F",
      "team": "CLE"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 1,
            "bookie": "FanDuel",
            "line": {
              "cost": 2.06,
              "line": 1.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 2,
            "bookie": "DraftKings",
            "line": {
              "cost": 1.77,
              "line": 1.5
            }
          },
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 1.77,
              "line": 1.5
            }
          },
          {
            "id": 19,
            "bookie": "Barstool",
            "line": {
              "cost": 1.77,
              "line": 1.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Points",
    "player": {
      "name": "Kyle Kuzma",
      "position": "F",
      "team": "WAS"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 4,
            "bookie": "PointsBet",
            "line": {
              "cost": 1.83,
              "line": 22.5
            }
          },
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 1.83,
              "line": 22.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 5,
            "bookie": "SugarHouse",
            "line": {
              "cost": 1.87,
              "line": 23.5
            }
          },
          {
            "id": 7,
            "bookie": "BetRivers",
            "line": {
              "cost": 1.87,
              "line": 23.5
            }
          },
          {
            "id": 14,
            "bookie": "UniBet",
            "line": {
              "cost": 1.87,
              "line": 23.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Blocks",
    "player": {
      "name": "Dean Wade",
      "position": "F",
      "team": "CLE"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 2.95,
              "line": 0.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 3,
            "bookie": "Caesars",
            "line": {
              "cost": 1.43,
              "line": 0.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Blocks",
    "player": {
      "name": "Tyus Jones",
      "position": "G",
      "team": "WAS"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 3.1,
              "line": 0.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 19,
            "bookie": "Barstool",
            "line": {
              "cost": 1.38,
              "line": 0.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Rebounds",
    "player": {
      "name": "Jordan Poole",
      "position": "G",
      "team": "WAS"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 2,
            "bookie": "DraftKings",
            "line": {
              "cost": 2.05,
              "line": 2.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 4,
            "bookie": "PointsBet",
            "line": {
              "cost": 1.77,
              "line": 2.5
            }
          },
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 1.77,
              "line": 2.5
            }
          },
          {
            "id": 19,
            "bookie": "Barstool",
            "line": {
              "cost": 1.77,
              "line": 2.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Steals",
    "player": {
      "name": "Corey Kispert",
      "position": "F",
      "team": "WAS"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 2.8,
              "line": 0.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 4,
            "bookie": "PointsBet",
            "line": {
              "cost": 1.44,
              "line": 0.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Steals",
    "player": {
      "name": "Bilal Coulibaly",
      "position": "F",
      "team": "WAS"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 8,
            "bookie": "BetMGM",
            "line": {
              "cost": 2.05,
              "line": 0.5
            }
          },
          {
            "id": 13,
            "bookie": "PartyCasino",
            "line": {
              "cost": 2.05,
              "line": 0.5
            }
          },
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 2.05,
              "line": 0.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 19,
            "bookie": "Barstool",
            "line": {
              "cost": 1.77,
              "line": 0.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Three Points Made",
    "player": {
      "name": "Deni Avdija",
      "position": "F",
      "team": "WAS"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 1,
            "bookie": "FanDuel",
            "line": {
              "cost": 2.76,
              "line": 1.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 10,
            "bookie": "bet365",
            "line": {
              "cost": 1.5,
              "line": 1.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Assists",
    "player": {
      "name": "Jarrett Allen",
      "position": "C",
      "team": "CLE"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 1,
            "bookie": "FanDuel",
            "line": {
              "cost": 2,
              "line": 3.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 4,
            "bookie": "PointsBet",
            "line": {
              "cost": 1.95,
              "line": 3.5
            }
          }
        ]
      }
    ]
  },
  {
    "market_label": "Points",
    "player": {
      "name": "Donovan Mitchell",
      "position": "G",
      "team": "CLE"
    },
    "selections": [
      {
        "label": "Over",
        "books": [
          {
            "id": 5,
            "bookie": "SugarHouse",
            "line": {
              "cost": 1.83,
              "line": 30.5
            }
          },
          {
            "id": 7,
            "bookie": "BetRivers",
            "line": {
              "cost": 1.83,
              "line": 30.5
            }
          },
          {
            "id": 14,
            "bookie": "UniBet",
            "line": {
              "cost": 1.83,
              "line": 30.5
            }
          }
        ]
      },
      {
        "label": "Under",
        "books": [
          {
            "id": 1,
            "bookie": "FanDuel",
            "line": {
              "cost": 1.85,
              "line": 31.5
            }
          }
        ]
      }
    ]
  }
]

names = []
for res in response:

    if res['player']['name'] not in names:
        names.append(res['player']['name'])

for name in names:
    print('-----')
    print(name)
    for res in response:
        if res['player']['name'] == name:
            print(res['market_label'], res['selections'][0]['books'][0]['line']['line'])
