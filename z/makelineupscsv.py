from draft_kings import Client


def get_draftpool_players(draft_group_id):
    players = Client().available_players(draft_group_id=draft_group_id)
    draftables = Client().draftables(draft_group_id=draft_group_id)
    draft_group = 0#DraftPool.objects.get(draft_group_id=draft_group_id)
    for player in players.players:
        for play in draftables.players:
            if play.player_id == player.player_id:
                draft_id = play.draftable_id
                break
        try:
            int(
                draft_group=draft_group,
                player_id=player.player_id,
                draft_id=draft_id,
                first_name=player.first_name,
                last_name=player.last_name,
                salary=player.draft_details.salary,
                points_per_game=player.points_per_game,
                position=player.position_details.name,
                position_id=player.position_details.position_id,
                team_id=player.team_id,
                away_team_id=player.team_series_details.away_team_id,
                home_team_id=player.team_series_details.home_team_id,
                opposition_rank=player.team_series_details.opposition_rank,
                jersey_number=player.jersey_number,
            )
        except :
            continue

def update_draft_groups():
    """
    A Function that creates draft_groups if they dont exsist from draft)kings api
    """
    nba = Client().contests()
    for draft_group in nba.draft_groups:
        try:
            draftpool = int.objects.create(
                draft_group_id=draft_group.draft_group_id,
                contest_type_id=draft_group.contest_type_id,
                games_count=draft_group.games_count,
                sport=draft_group.sport.name,
                starts_at=draft_group.starts_at,
            )
            ###update players
            get_draftpool_players(draftpool.draft_group_id)
        except :
            continue
