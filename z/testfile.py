from pulp import LpProblem, LpMaximize, LpVariable, lpSum
import pandas as pd

def optimize_lineups_with_players(selected_players, player_data, num_lineups=10, salary_cap=50000):
    all_lineups = []

    for _ in range(num_lineups):
        # Set up the problem
        model = LpProblem(name="DraftKings_Lineup_Optimization", sense=LpMaximize)

        # Decision variables
        players = LpVariable.dicts("Player", player_data.keys(), cat="Binary")

        # Objective function: Maximize total projected points
        model += lpSum(players[player] * player_data[player]['AvgPointsPerGame'] for player in player_data.keys())

        # Constraint: Stay within the salary cap
        model += lpSum(players[player] * player_data[player]['Salary'] for player in player_data.keys()) <= salary_cap

        # Constraint: Number of players in the lineup
        model += lpSum(players[player] for player in player_data.keys()) == 6

        # Include the specified players in the lineup
        for player in selected_players:
            model += players[player] == 1

        # Exclude players already selected in previous lineups
        if all_lineups:
            for lineup in all_lineups:
                model += lpSum(players[player] for player in lineup) <= 5

        # Solve the problem
        model.solve()

        # Check if the optimization was successful
        if model.status == 1:
            selected_players = [player for player in player_data.keys() if players[player].varValue == 1]
            total_points = sum(player_data[player]['AvgPointsPerGame'] for player in selected_players)
            total_salary = sum(player_data[player]['Salary'] for player in selected_players)

            all_lineups.append(selected_players)

    return all_lineups

# Example usage
# Replace 'path/to/your/file.csv' with the actual path to your CSV file
csv_file_path = 'DKSalaries (4).csv'
player_df = pd.read_csv(csv_file_path)

# Convert the dataframe to a dictionary for easier access
player_data = player_df.set_index('Name + ID').to_dict(orient='index')

# Specify up to 5 players
specified_players = ['Malcolm Brogdon (31504063)','Scoot Henderson (31504237)']

# Get 10 eligible lineups that include the specified players
eligible_lineups = optimize_lineups_with_players(specified_players, player_data, num_lineups=10)

# Display the results
for idx, lineup in enumerate(eligible_lineups, 1):
    total_points = sum(player_data[player]['AvgPointsPerGame'] for player in lineup)
    total_salary = sum(player_data[player]['Salary'] for player in lineup)

    print(f"Lineup {idx} - Total Points: {total_points}, Total Salary: {total_salary}")
    print("Selected Players:", lineup)
    print("\n")
