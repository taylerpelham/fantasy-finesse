import requests

"""
"http://site.api.espn.com/apis/site/v2/sports/basketball/nba/summary?event=401585112"
dict_keys([
    'boxscore', 'format', 'gameInfo',
    'lastFiveGames', 'leaders', 'seasonseries',
    'injuries', 'broadcasts', 'predictor', 'pickcenter',
    'againstTheSpread', 'odds', 'news', 'videos',
    'header', 'ticketsInfo', 'standings'])

"http://site.api.espn.com/apis/site/v2/sports/basketball/nba/scoreboard"
dict_keys(['leagues', 'season', 'day', 'events'])


Espn link to player
https://www.espn.com/nba/player/_/id/3032977/giannis-antetokounmpo
Espn link to team
https://www.espn.com/nba/team/_/name/cha/charlotte-hornets

"http://site.api.espn.com/apis/site/v2/sports/basketball/nba/teams/30"
# gets players thur 'athletes'
"http://site.api.espn.com/apis/site/v2/sports/basketball/nba/teams/30?enable=roster"


https://site.web.api.espn.com/apis/site/v2/sports/{sport}/{league}/athletes/{id}:

"""

url = "http://site.api.espn.com/apis/site/v2/sports/basketball/nba/statistics/player/3032977"

try:
    # Make a GET request to the URL
    response = requests.get(url)
    # Check if the request was successful (status code 200)
    if response.status_code == 200:
        # Print the response content
        data = response.json()

        print(data['team']['athletes'][:1])
    else:
        print(f"Error: {response.status_code}")
except Exception as e:
    print(f"An error occurred: {e}")

"""
dict_keys([
    'boxscore', 'format', 'gameInfo',
    'lastFiveGames', 'leaders', 'seasonseries',
    'injuries', 'broadcasts', 'predictor', 'pickcenter',
    'againstTheSpread', 'odds', 'news', 'videos',
    'header', 'ticketsInfo', 'standings'])
"""
