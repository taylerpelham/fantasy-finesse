import csv
from operator import itemgetter
# csv_file_path = 'DKSalaries (4).csv'

players_dict = {}
with open('DKSalaries (5).csv', newline='') as csvfile:
    reader = csv.DictReader(csvfile)
    for player in reader:
            try:
                x = int(player['Salary'])/float(player['AvgPointsPerGame'])
                player['DollarPerPoint'] = round(x,2)
                players_dict[player['DollarPerPoint']] = player
            except ZeroDivisionError:
                pass
sorted_dict = dict(sorted(players_dict.items()))

#print(sorted_dict.keys()) #print(players_dict[423.73]['Name'])
for player in sorted_dict.keys():
    y = players_dict[player]
    #y = players_dict[player]
    x = f"{y['DollarPerPoint']}:::{(y)['Roster Position']}:::{y['Name']}:::{y['Salary']} AVGPPG:{y['AvgPointsPerGame']}"
    print(x)
