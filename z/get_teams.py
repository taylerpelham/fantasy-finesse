import http.client
import json
conn = http.client.HTTPSConnection("api-nba-v1.p.rapidapi.com")

headers = {
    'x-rapidapi-host': "api-nba-v1.p.rapidapi.com",
    'x-rapidapi-key': "b99d40bbc6msh3aa3e31f97d65b8p1d29ddjsnbf5d620b0880",
    }

conn.request("GET", "/teams", headers=headers)

res = conn.getresponse()
data = res.read()

dict1 = json.loads(data.decode("utf-8"))

#keys for dict1['response'][0]
#dict_keys(['id', 'name', 'nickname', 'code', 'city', 'logo', 'allStar', 'nbaFranchise', 'leagues'])

for response in dict1['response']:
    if response['nbaFranchise'] and response['name'] != 'Home Team Stephen A':
        print(response['id'])
        print(response['name'])
        print(response['code'])
        print(response['nickname'])
        print(response['logo'])
        print(response['city'])
        print(response['leagues']['standard']['conference'])
        print(response['leagues']['standard']['division'])
