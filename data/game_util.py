import http.client
import json
from time import sleep
from player_ids import defense_rank
def get_home_team_code(game_id):
    sleep(6) # free trail only llows 10 requests per min
    api_key = 'b99d40bbc6msh3aa3e31f97d65b8p1d29ddjsnbf5d620b0880'

    conn = http.client.HTTPSConnection("api-nba-v1.p.rapidapi.com")

    headers = {
        'x-rapidapi-host': "api-nba-v1.p.rapidapi.com",
        'x-rapidapi-key': api_key
        }
    conn.request("GET", f"/games?id={game_id}", headers=headers)
    res = conn.getresponse()
    data = res.read()
    game_data = json.loads(data.decode("utf-8"))
    if game_data:
        try:
            home_team_code = game_data['response'][0]['teams']['home']['code']
            visitors_team_code = game_data['response'][0]['teams']['visitors']['code']
            return home_team_code, defense_rank[visitors_team_code]
        except KeyError:
            return None
