### Current this is a page that returns the team id by its 3 digit code ex: alt, den

team_abbv = 'atl'

api_key = 'b99d40bbc6msh3aa3e31f97d65b8p1d29ddjsnbf5d620b0880'
import http.client

conn = http.client.HTTPSConnection("api-nba-v1.p.rapidapi.com")

headers = {
    'x-rapidapi-host': "api-nba-v1.p.rapidapi.com",
    'x-rapidapi-key': api_key
    }

conn.request("GET", f"/teams?code={team_abbv}", headers=headers)

res = conn.getresponse()
data = res.read()
print(data.decode("utf-8"))
