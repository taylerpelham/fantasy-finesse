import joblib
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_absolute_error
from sklearn.preprocessing import StandardScaler
import pandas as pd
import numpy as np

loaded_model = joblib.load('linear_regression_model3.pkl')

# Load the scaler
loaded_scaler = joblib.load('standard_scaler3.pkl')

new_data = pd.DataFrame({'Game ID': [13871],'Home/away':[1], 'Def_rank': [7]})

new_data_array = np.array(new_data).reshape(1, -1)
# Assuming 'new_data' is a DataFrame with the same structure as your training data
new_data_scaled = loaded_scaler.transform(new_data)

# Make predictions on the new data
new_predictions = loaded_model.predict(new_data_scaled)
print(new_predictions)
