import http.client

api_key = 'b99d40bbc6msh3aa3e31f97d65b8p1d29ddjsnbf5d620b0880'
conn = http.client.HTTPSConnection("api-nba-v1.p.rapidapi.com")

headers = {
    'x-rapidapi-host': "api-nba-v1.p.rapidapi.com",
    'x-rapidapi-key': api_key
    }

conn.request("GET", "/players?team=30&season=2023", headers=headers)

res = conn.getresponse()
data = res.read()

import json

def parse_json_and_create_dict(json_response):
    # Parse the JSON response
    response_data = json.loads(json_response)

    # Check if 'response' key is present
    if 'response' in response_data:
        # Extract the list of players from the 'response' key
        players_data = response_data['response']

        # Create an empty dictionary to store player_name: player_id pairs
        players_dict = {}

        # Iterate through the player data and populate the dictionary
        for player in players_data:
            player_name = f"{player['firstname']} {player['lastname']}"
            player_id = player['id']

            # Add the player_name: player_id pair to the dictionary
            players_dict[player_name] = player_id

        return players_dict
    else:
        print("Error: 'response' key not found in JSON.")


# Parse the JSON and create the dictionary
resulting_dict = parse_json_and_create_dict(data.decode("utf-8"))

# Print the resulting dictionary
print(resulting_dict)
