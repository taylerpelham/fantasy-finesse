## build a function that takes a csv as input and returns a list a of player_ids using the players dict
from player_ids import player_ids
import http.client
import csv
import json
from game_util import get_home_team_code

player = 'De\'Aaron Fox'
api_key = 'b99d40bbc6msh3aa3e31f97d65b8p1d29ddjsnbf5d620b0880'
id = player_ids[player]
game_stats = [
                ["Game ID", "Points", "Minutes", "FGM", "FGA", "FG%", "FTM", "FTA", "FT%", "3PM", "3PA", "3P%",
                 "OffReb", "DefReb", "TotReb", "Assists", "PFouls", "Steals", "Turnovers", "Blocks", "Plus/Minus",
                 'Home/away', 'Def_rank'],
            ]
if id:
    years = [2023]

    for year in years:
        api_key = 'b99d40bbc6msh3aa3e31f97d65b8p1d29ddjsnbf5d620b0880'

        conn = http.client.HTTPSConnection("api-nba-v1.p.rapidapi.com")

        headers = {
            'x-rapidapi-host': "api-nba-v1.p.rapidapi.com",
            'x-rapidapi-key': api_key
        }

        conn.request("GET", f"/players/statistics?season={year}&id={id}", headers=headers)

        if conn:
            res = conn.getresponse()
            data = res.read()

            data.decode("utf-8")

            player_dict = json.loads(data.decode("utf-8"))

            try:
                player_dict["response"][0]

                player_team = player_dict["response"][0]['team']['code']

                games_appended = 0  # Counter for games appended to game_stats

                for entry in player_dict["response"]:
                    home_team_den_rank = get_home_team_code(entry["game"]["id"])
                    print(home_team_den_rank)

                    if home_team_den_rank:
                        home_team, den_rank = home_team_den_rank
                        if home_team == player_dict["response"][0]['team']['code']:
                            home_away = 1
                        else:
                            home_away = 0

                        game_stats.append([
                            entry["game"]["id"],
                            entry["points"],
                            entry["min"],
                            entry["fgm"],
                            entry["fga"],
                            entry["fgp"],
                            entry["ftm"],
                            entry["fta"],
                            entry["ftp"],
                            entry["tpm"],
                            entry["tpa"],
                            entry["tpp"],
                            entry["offReb"],
                            entry["defReb"],
                            entry["totReb"],
                            entry["assists"],
                            entry["pFouls"],
                            entry["steals"],
                            entry["turnovers"],
                            entry["blocks"],
                            entry["plusMinus"],
                            home_away,
                            den_rank,
                        ])

                        games_appended += 1
                        print(games_appended)

            except KeyError:
                continue
    csv_file_path = f"{player}_game_stats2.csv"
    with open(csv_file_path, mode="w", newline="", encoding="utf-8") as csv_file:
        csv_writer = csv.writer(csv_file)
        csv_writer.writerows(game_stats)
else:
    print('Invalid ID')

print(f"Game statistics for {player} have been saved to {csv_file_path}")
