from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_absolute_error
from sklearn.preprocessing import StandardScaler
import pandas as pd
import joblib
# Assuming you have a DataFrame called 'player_data' with features and target variable
file_path = 'De\'Aaron Fox_game_stats.csv'
player_data = pd.read_csv(file_path)

#columns from your CSV

X = player_data[['Game ID','Home/away']]#'Home/away', 'Def_rank'
y = player_data[["Points"]]
#"Minutes", "FGM", "FGA", "FTM", "FTA", "3PM", "3PA", "OffReb", "DefReb", "TotReb", "Assists", "Steals", "Blocks"
# Split the data into training and testing sets
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Standardize features
scaler = StandardScaler()
X_train_scaled = scaler.fit_transform(X_train)
X_test_scaled = scaler.transform(X_test)

# Choose a model (Linear Regression in this case)
model = LinearRegression()

# Train the model
model.fit(X_train_scaled, y_train)

# Make predictions on the test set
predictions = model.predict(X_test_scaled)

# Evaluate the model
mae = mean_absolute_error(y_test, predictions)
print(f"Mean Absolute Error: {mae}")

model_filename = 'linear_regression_model3.pkl'
joblib.dump(model, model_filename)

scaler_filename = 'standard_scaler3.pkl'
joblib.dump(scaler, scaler_filename)
