import http.client

api_key = 'b99d40bbc6msh3aa3e31f97d65b8p1d29ddjsnbf5d620b0880'
conn = http.client.HTTPSConnection("api-nba-v1.p.rapidapi.com")

headers = {
    'x-rapidapi-host': "api-nba-v1.p.rapidapi.com",
    'x-rapidapi-key': api_key
    }

conn.request("GET", "/games?id=13764", headers=headers)

res = conn.getresponse()
data = res.read()

print(data.decode("utf-8"))
