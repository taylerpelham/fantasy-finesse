import os
import django
from django.core.management import call_command

def run_shell_commands():
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'fantasy_finesse.settings')  # Replace 'your_project.settings' with your actual project's settings module

    django.setup()

    from lineups.utils import update_draftpools
    update_draftpools()

if __name__ == '__main__':
    run_shell_commands()