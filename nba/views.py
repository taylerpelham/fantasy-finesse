from django.shortcuts import render, get_object_or_404
from .models import Player, Team, PlayerStat
from .utils import predict_next_game
from django.db.models import OuterRef, Subquery
# Create your views here.


def player_detail(request, id):
    player = get_object_or_404(Player, id=id)
    try:
        predict = predict_next_game(id)
        points = round(predict[0])
        if points < 0:
            points = 'Could not predict'
        prediction = round(predict[1],2)
        if prediction < 0:
            prediction = 'Could not predict'
        rebounds = round(predict[2])
        if rebounds < 0:
            rebounds = 'Could not predict'
        assists = round(predict[3])
        if assists < 0:
            assists = 'Could not predict'
    except:
        prediction = "Invalid Data"
    stats = PlayerStat.objects.filter(player=player).order_by('game')[:30]
    context = {
        'player': player,
        'prediction': prediction if prediction else 'Could not predict',
        'stats': stats,
        'rebounds': rebounds if rebounds else 'Could not predict',
        'assists': assists if assists else 'Could not predict',
        'points': points if points else 'Could not predict',
        }
    return render(request, 'nba/player_detail.html', context)


def team_detail(request, id):
    team = get_object_or_404(Team, id=id)
    # the next 2 lines get all players and order them by their last playerstat date
    latest_game_date_subquery = PlayerStat.objects.filter(player=OuterRef('pk')).order_by('-game__date_played').values('game__date_played')[:1]
    players = Player.objects.filter(team=team).annotate(latest_game_date=Subquery(latest_game_date_subquery)).order_by('-latest_game_date')
    context = {'team': team, 'players': players}
    return render(request, 'nba/team_detail.html', context)


def team_list(request):
    teams = Team.objects.all()
    context = {'teams': teams}
    return render(request, 'nba/team_list.html', context)
