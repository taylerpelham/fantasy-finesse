from django.db import models


class Team(models.Model):
    id = models.BigIntegerField(primary_key=True)
    name = models.CharField(max_length=100)
    code = models.CharField(max_length=100)
    nickname = models.CharField(max_length=100)
    conference = models.CharField(max_length=100)
    division = models.CharField(max_length=100)
    logo = models.URLField(blank=True, null=True)
    city = models.CharField(max_length=100)

    def __str__(self):
        return f"{self.name}"


class Player(models.Model):
    id = models.BigIntegerField(primary_key=True)
    dk_player_id = models.IntegerField(default=0, null=True, blank=True)
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    jersey_num = models.IntegerField(null=True, blank=True)
    position = models.CharField(max_length=10, null=True, blank=True)
    picture = models.URLField(null=True, blank=True)
    team = models.ForeignKey(Team, related_name='players', on_delete=models.SET_NULL, blank=True, null=True)

    def __str__(self):
        return f"{self.first_name} {self.last_name}"


class Game(models.Model):
    id = models.BigIntegerField(primary_key=True)
    home_team = models.ForeignKey(Team, on_delete=models.SET_NULL, related_name='home_team', null=True, blank=True)
    away_team = models.ForeignKey(Team, on_delete=models.SET_NULL, related_name='away_team', null=True, blank=True)
    date_played = models.DateTimeField(null=True, blank=True)
    season = models.IntegerField(default=0)
    visitor_score = models.IntegerField(default=0)
    home_score = models.IntegerField(default=0)
    ###possible to get linescores, 1st 2nd 3rd 4th quarter scores they are lists

    class Meta:
        ordering = ['-date_played']

    def __str__(self):
        return f'{self.id}'


class PlayerStat(models.Model):
    player = models.ForeignKey(Player, related_name="playerstats", on_delete=models.CASCADE)
    game = models.ForeignKey(Game, related_name="playerstats", on_delete=models.SET_NULL, null=True, blank=True)
    team = models.ForeignKey(Team, related_name="playerstats", on_delete=models.SET_NULL, null=True, blank=True)
    is_home_game = models.BooleanField(null=True, blank=True)
    points = models.IntegerField(default=0, null=True, blank=True)
    position = models.CharField(max_length=20, null=True, blank=True)
    minutes = models.CharField(max_length=10, null=True, blank=True)
    fgm = models.IntegerField(default=0, null=True, blank=True)
    fga = models.IntegerField(default=0, null=True, blank=True)
    fgp = models.CharField(max_length=10, null=True, blank=True)
    ftm = models.IntegerField(default=0, null=True, blank=True)
    fta = models.IntegerField(default=0, null=True, blank=True)
    ftp = models.CharField(max_length=10, null=True, blank=True)
    tpm = models.IntegerField(default=0, null=True, blank=True)
    tpa = models.IntegerField(default=0, null=True, blank=True)
    tpp = models.CharField(max_length=10, null=True, blank=True)
    off_rebound = models.IntegerField(default=0, null=True, blank=True)
    def_rebound = models.IntegerField(default=0, null=True, blank=True)
    total_rebound = models.IntegerField(default=0, null=True, blank=True)
    assists = models.IntegerField(default=0, null=True, blank=True)
    personal_fouls = models.IntegerField(default=0, null=True, blank=True)
    steals = models.IntegerField(default=0, null=True, blank=True)
    turnovers = models.IntegerField(default=0, null=True, blank=True)
    blocks = models.IntegerField(default=0, null=True, blank=True)
    plus_minus = models.CharField(max_length=10, null=True, blank=True)
    fantasy_points = models.FloatField(default=0, null=True, blank=True)

    def save(self, *args, **kwargs):
        # Auto Update is_home_game
        if self.game and self.team:
            self.is_home_game = self.team == self.game.home_team
        # Auto Update fantasy_points
        fantasy_points = (
            self.points +
            self.tpm * 0.5 +  # Made 3pt Shot
            self.total_rebound * 1.25 +  # Rebound
            self.assists * 1.5 +  # Assist
            self.steals * 2 +  # Steal
            self.blocks * 2 -  # Block
            self.turnovers * 0.5  # Turnover
        )

        # Check for Doubles
        double_count = 0
        if self.points >= 10:
            double_count += 1
        if self.total_rebound >= 10:
            double_count += 1
        if self.assists >= 10:
            double_count += 1
        if self.blocks >= 10:
            double_count += 1
        if self.steals >= 10:
            double_count += 1

        # Add bonus points for Double-Double
        if double_count >= 2:
            fantasy_points += 1.5

        # Add bonus points for Triple-Double
        if double_count >= 3:
            fantasy_points += 3

        self.fantasy_points = fantasy_points

        super(PlayerStat, self).save(*args, **kwargs)

    def __str__(self):
        return f"{self.player} stats for {self.game}"

    class Meta:
        unique_together = ['player','game']
