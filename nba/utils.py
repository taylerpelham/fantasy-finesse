from .models import Team, Player, Game, PlayerStat
import http.client
import json
from django.shortcuts import render, get_object_or_404
import django
import nba
from datetime import datetime
import pandas as pd
import numpy as np
from django_pandas.io import read_frame
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_absolute_error
from sklearn.preprocessing import StandardScaler
from lineups.models import DraftPlayer
### These utils can be used to import to the database


"""
Create utils for predict, the idea is to be able to call a single function which is passed Player
and returns the prediction, Maybe we will create a model to store predictions.
"""


def predict_next_game(player_id):
    """
    Currently takes player_id as input and returns Points prediction,
    NEEDS: retrieve next_game data. this will need to be able to be referenced from Player
    """

    player = get_object_or_404(Player, id=player_id)
    player_stats = PlayerStat.objects.filter(player=player)[:30]
    player_data = read_frame(player_stats)

    X = player_data[['game', 'is_home_game']]  # Model Input
    y = player_data[['points', 'fantasy_points', 'total_rebound', 'assists']] #Model Output

    feature_names = X.columns

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

    scaler = StandardScaler()
    X_train_scaled = scaler.fit_transform(X_train)
    X_test_scaled = scaler.transform(X_test)

    # Choose a model
    model = LinearRegression()

    # Train model
    model.fit(X_train_scaled, y_train)

    # Make predictions on the test set
    predictions = model.predict(X_test_scaled)

    # test accuratly
    mae = mean_absolute_error(y_test, predictions)

    ### NEEDS: as of rn this is manaul input for Next game
    next_game_features = [[13001, False]]  # Replace with actual features
    next_game_features_scaled = scaler.transform(next_game_features)

    next_game_prediction = model.predict(next_game_features_scaled)
    print(next_game_prediction)
    return next_game_prediction[0]


def get_players_stats_from_game_id(game_id):
    conn = http.client.HTTPSConnection("api-nba-v1.p.rapidapi.com")
    headers = {
        'X-RapidAPI-Key': "b99d40bbc6msh3aa3e31f97d65b8p1d29ddjsnbf5d620b0880",
        'X-RapidAPI-Host': "api-nba-v1.p.rapidapi.com"
    }
    conn.request("GET", f"/players/statistics?game={game_id}", headers=headers)
    res = conn.getresponse()
    data = res.read()
    dict1 = json.loads(data.decode("utf-8"))
    return dict1

get_players_stats_from_game_id
def create_playerstat_from_response_dict(dict1):
    """
    Takes Player Stats Dict as input
    """
    for response in dict1['response']:
        team_id=response['team']['id']
        #get team
        try:
          team = get_object_or_404(Team, id=team_id)
        except django.db.utils.IntegrityError and django.http.response.Http404:
            continue
        #get player
        player_id=response['player']['id']
        try:
          player = get_object_or_404(Player, id=player_id)
        except django.http.response.Http404:
            player = Player.objects.create(
                id=response["player"]['id'],
                first_name=response["player"]['firstname'],
                last_name=response["player"]['lastname'],
                position=response['pos'],
                team=team,
                )
        #get game
        game_id=response['game']['id']
        try:
          game = get_object_or_404(Game, id=game_id)
        except django.db.utils.IntegrityError and django.http.response.Http404:
            continue
        #create PlayerStat
        try:
            PlayerStat.objects.create(
            player=player,
            game=game,
            team=team,
            points=response['points'],
            position=response['pos'],
            minutes=response['min'],
            fgm=response['fgm'],
            fga=response['fga'],
            fgp=response['fgp'],
            ftm=response['ftm'],
            fta=response['fta'],
            ftp=response['ftp'],
            tpm=response['tpm'],
            tpa=response['tpa'],
            tpp=response['tpp'],
            off_rebound=response['offReb'],
            def_rebound=response['defReb'],
            total_rebound=response['totReb'],
            assists=response['assists'],
            personal_fouls=response['pFouls'],
            steals=response['steals'],
            turnovers=response['turnovers'],
            blocks=response['blocks'],
            plus_minus=response['plusMinus'],
            )
        except django.db.utils.IntegrityError:
            continue

def get_games_from_season(season):
    conn = http.client.HTTPSConnection("api-nba-v1.p.rapidapi.com")
    headers = {
        'X-RapidAPI-Key': "b99d40bbc6msh3aa3e31f97d65b8p1d29ddjsnbf5d620b0880",
        'X-RapidAPI-Host': "api-nba-v1.p.rapidapi.com"
    }
    conn.request("GET", f"/games?season={season}", headers=headers)
    res = conn.getresponse()
    data = res.read()
    dict1 = json.loads(data.decode("utf-8"))
    return dict1


def get_games_from_date(date=datetime.today().strftime('%Y-%m-%d')):
    """
    returns a python dict of all games played on the pasted in date,
    default date is the current day
    """
    conn = http.client.HTTPSConnection("api-nba-v1.p.rapidapi.com")
    headers = {
        'X-RapidAPI-Key': "b99d40bbc6msh3aa3e31f97d65b8p1d29ddjsnbf5d620b0880",
        'X-RapidAPI-Host': "api-nba-v1.p.rapidapi.com"
    }
    conn.request("GET", f"/games?date={date}", headers=headers)
    res = conn.getresponse()
    data = res.read()
    dict1 = json.loads(data.decode("utf-8"))
    return dict1


def create_game_fr_response(game_dict):
    if game_dict["league"] == "standard":
        try:
            home_team_id = game_dict['teams']["home"]["id"],
            away_team_id = game_dict['teams']["visitors"]["id"],
            home_team = get_object_or_404(Team, id=home_team_id[0])
            away_team = get_object_or_404(Team, id=away_team_id[0])
            Game.objects.create(
            id=game_dict['id'],
            home_team=home_team,
            away_team=away_team,
            date_played=game_dict['date']['start'],
            season=game_dict["season"],
            visitor_score=game_dict['scores']["visitors"]["points"],
            home_score=game_dict['scores']["home"]["points"],
            )
        except nba.models.Team.DoesNotExist and django.http.response.Http404:
            pass

def create_games_loop(games_dict):
    for game in games_dict['response']:
        try:
            create_game_fr_response(game)
        except django.contrib.admin.exceptions.AlreadyRegistered and django.db.utils.IntegrityError:
            continue



def get_players_from_team(team):
    conn = http.client.HTTPSConnection("api-nba-v1.p.rapidapi.com")
    headers = {
        'X-RapidAPI-Key': "b99d40bbc6msh3aa3e31f97d65b8p1d29ddjsnbf5d620b0880",
        'X-RapidAPI-Host': "api-nba-v1.p.rapidapi.com"
    }
    conn.request("GET", f"/players?team={team}&season=2023", headers=headers)
    res = conn.getresponse()
    data = res.read()
    team_players_dict = json.loads(data.decode("utf-8"))
    return team_players_dict


def create_players_from_team_dict(team_players_dict, team_id):
    team = get_object_or_404(Team, id=team_id)
    for response in team_players_dict['response']:
        try:
            if response['leagues']['standard']['jersey']:
                Player.objects.create(
                id=response['id'],
                first_name=response['firstname'],
                last_name=response['lastname'],
                jersey_num=response['leagues']['standard']['jersey'],
                position=response['leagues']['standard']['pos'],
                team=team,
                )
            else:
                Player.objects.create(
                id=response['id'],
                first_name=response['firstname'],
                last_name=response['lastname'],
                position=response['leagues']['standard']['pos'],
                team=team,
                )
        except django.db.utils.IntegrityError:
            continue


def create_players_loop(team_ids):
    #loop to mass create Player objects
    for team in team_ids:
        dict1 = get_players_from_team(team)
        create_players_from_team_dict(dict1, team)


def get_teams_dict():
    conn = http.client.HTTPSConnection("api-nba-v1.p.rapidapi.com")
    headers = {
        'x-rapidapi-host': "api-nba-v1.p.rapidapi.com",
        'x-rapidapi-key': "b99d40bbc6msh3aa3e31f97d65b8p1d29ddjsnbf5d620b0880",
        }
    conn.request("GET", "/teams", headers=headers)
    res = conn.getresponse()
    data = res.read()
    dict1 = json.loads(data.decode("utf-8"))
    return dict1


#keys for dict1['response'][0]
#dict_keys(['id', 'name', 'nickname', 'code', 'city', 'logo', 'allStar', 'nbaFranchise', 'leagues'])
def get_teams(dict1):
    for response in dict1['response']:
        if response['nbaFranchise'] and response['name'] != 'Home Team Stephen A':
            Team.objects.create(
            id=response['id'],
            name=response['name'],
            code=response['code'],
            nickname=response['nickname'],
            logo=response['logo'],
            city=response['city'],
            conference=response['leagues']['standard']['conference'],
            division=response['leagues']['standard']['division'],
            )


def update_nba_daily(date=datetime.today().strftime('%Y-%m-%d')):
    print(date)
    games = get_games_from_date(date)
    create_games_loop(games)
    for game in games['response']:
        pstats = get_players_stats_from_game_id(game['id'])
        create_playerstat_from_response_dict(pstats)


'''
write a fucntion that attempts to add playerid from draftplayer to Player in NBA
'''
def assign_ids():
    dps = DraftPlayer.objects.all()

    for dp in dps:
        dp_names = dp.name.split()
        if len(dp_names) > 1:
            try:
                matching = Player.objects.get(first_name=dp_names[0], last_name=dp_names[1])
            except nba.models.Player.DoesNotExist:
                continue
        if matching:
            matching.dk_player_id = dp.player_id
            matching.save()
