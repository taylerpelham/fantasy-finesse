from django.contrib import admin
from .models import Team, Player, Game, PlayerStat
# Register your models here.

@admin.register(Team)
class TeamAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'code']

@admin.register(Player)
class PlayerAdmin(admin.ModelAdmin):
    list_display = ['id', 'first_name', 'last_name', 'position', 'team', 'dk_player_id']

@admin.register(Game)
class GameAdmin(admin.ModelAdmin):
    list_display = ['id', 'home_team', 'away_team']

@admin.register(PlayerStat)
class PlayerStatAdmin(admin.ModelAdmin):
    list_display = ['player', 'points', 'game', 'team', 'is_home_game']
