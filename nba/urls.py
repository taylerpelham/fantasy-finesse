from django.urls import path
from .views import team_list, team_detail, player_detail


urlpatterns = [
    path('teams/', team_list, name='team_list'),
    path('teams/<int:id>/', team_detail, name='team_detail'),
    path('player/<int:id>/', player_detail, name="player_detail"),
]
