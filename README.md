# Fantasy-Finesse

## Getting started

1. **Fork and Clone:**
   - Fork this repository by clicking on the "Fork" button in the top right corner.
   - Clone the forked repository onto your local computer:

     ```bash
     git clone <YOUR_FORKED_REPO_URL>
     cd fantasy-finesse
     ```

2. **Set Up Virtual Environment:**
   - Create and activate a virtual environment:

     ```bash
     python -m venv .venv
     # Windows
     .\.venv\Scripts\Activate
     # macOS/Linux
     source .venv/bin/activate
     ```

3. **Install Dependencies:**
   - Install the project dependencies:

     ```bash
     pip install -r requirements.txt
     ```

4. **Update the DB:**
     ```bash
     python update_db.py
     ```

5. **Run the Server:**
   - Start the development server:

     ```bash
     python manage.py runserver
     ```

6. **View in Browser:**
   - Open your browser and navigate to [http://localhost:8000/](http://localhost:8000/) to view the project.

Now you're ready to explore Fantasy Finesse on your local machine!



***
## Name
Fantasy Finesse

## Description
Welcome to Fantasy Finesse, your go-to destination for mastering daily fantasy sports! As the creator of this dynamic Django web application, I present to you a powerful tool meticulously crafted for fantasy sports enthusiasts. Fantasy Finesse seamlessly integrates with the DraftKings API, extracting real-time player data to provide you with the most up-to-date information for your daily fantasy contests. This project serves as a testament to my skills and passion for web development, designed as a standout addition to my resume. With Fantasy Finesse, you have the ability to customize and crunch fantasy sports lineups based on your unique preferences and strategies. Whether you're a seasoned player or just starting your fantasy sports journey, Fantasy Finesse empowers you to make informed decisions and elevate your daily fantasy gaming experience. Explore the features, enjoy the intuitive interface, and take your fantasy sports finesse to new heights!

### Instructions
Select Draft Pool: Begin by choosing your preferred draft pool from the available options. Explore a diverse selection of players to curate a lineup that aligns with your fantasy sports strategy.
Customize Player Options: Fine-tune your lineup with precision by utilizing Fantasy Finesse's robust customization features. Include or exclude specific players based on your preferences. Set minimum and maximum exposure levels to ensure a well-balanced and strategic lineup.
Download CSV and Upload to DraftKings: Once you've perfected your fantasy lineup, effortlessly download the CSV file containing your selected players and their associated data. Seamlessly upload this file to your DraftKings account, saving you time and ensuring a smooth transition from Fantasy Finesse to the actual game.


## Authors and acknowledgment
The cruncher logic is imported from draftfast https://github.com/BenBrostoff/draftfast authored by Ben Brostoff.

## Project status
Time Became a constraint for this project, currently this project supports NBA and NFL with no plans to develop further.
